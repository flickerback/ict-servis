﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class ServisiranjePojedinacnoForm : Form
    {
        public ServisiranjePojedinacnoForm()
        {
            InitializeComponent();
            servisiranjeTableAdapter.Adapter.RowUpdated += Adapter_RowUpdated;
        }
        void Adapter_RowUpdated(object sender, System.Data.OleDb.OleDbRowUpdatedEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {
                var cmd = new OleDbCommand("SELECT @@IDENTITY", e.Command.Connection);
                var id = (int)cmd.ExecuteScalar();
                e.Row["ID"] = id;
                e.Row.AcceptChanges();
                foreach (var pk in ictServisDataSet1.ServisiranjeKomponenta)
                {
                    pk.IDServisiranje = id;
                }
            }
        }

        public int Id { get; set; }

        private void ServisiranjePojedinacnoForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ictServisDataSet1.OsobaKlijent' table. You can move, or remove it, as needed.
            this.osobaKlijentTableAdapter.Fill(this.ictServisDataSet1.OsobaKlijent);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Usluga' table. You can move, or remove it, as needed.
            this.uslugaTableAdapter.Fill(this.ictServisDataSet1.Usluga);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Uredaj' table. You can move, or remove it, as needed.
            this.uredajTableAdapter.Fill(this.ictServisDataSet1.Uredaj);
            // TODO: This line of code loads data into the 'ictServisDataSet1.OsobaZaposlenik' table. You can move, or remove it, as needed.
            this.osobaZaposlenikTableAdapter.Fill(this.ictServisDataSet1.OsobaZaposlenik);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Komponenta' table. You can move, or remove it, as needed.
            this.komponentaTableAdapter.Fill(this.ictServisDataSet1.Komponenta);
            if (Id == 0)
            {
                var red = ictServisDataSet1.Servisiranje.NewServisiranjeRow();
                red.DatumPredaje = DateTime.Today;
                red.DatumZavrsetka = DateTime.Today.AddDays(3);
                ictServisDataSet1.Servisiranje.AddServisiranjeRow(red);
            }
            else
            {
                servisiranjeTableAdapter.FillById(ictServisDataSet1.Servisiranje, Id);
                servisiranjeKomponentaTableAdapter.FillByServisiranje(ictServisDataSet1.ServisiranjeKomponenta, Id);
            }

        }

        private void tsb_spremi_Click(object sender, EventArgs e)
        {
            servisiranjeBindingSource.EndEdit();
            servisiranjeServisiranjeKomponentaBindingSource.EndEdit();
            servisiranjeTableAdapter.Update(ictServisDataSet1.Servisiranje);
            servisiranjeKomponentaTableAdapter.Update(ictServisDataSet1.ServisiranjeKomponenta);
            DialogResult = DialogResult.OK;
            Close();

        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
    }
}
