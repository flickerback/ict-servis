﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class VrsteUredajaForm : Form
    {
        public VrsteUredajaForm()
        {
            InitializeComponent();
            vrstaUredajaTableAdapter.Adapter.RowUpdated += Adapter_RowUpdated;

        }
        void Adapter_RowUpdated(object sender, System.Data.OleDb.OleDbRowUpdatedEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {
                var cmd = new OleDbCommand("SELECT @@IDENTITY", e.Command.Connection);
                var id = (int)cmd.ExecuteScalar();
                e.Row["ID"] = id;
                e.Row.AcceptChanges();
            }
        }

        private void VrsteUredaja_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ictServisDataSet1.VrstaUredaja' table. You can move, or remove it, as needed.
            this.vrstaUredajaTableAdapter.Fill(this.ictServisDataSet1.VrstaUredaja);

        }

        private void tsb_spremi_Click(object sender, EventArgs e)
        {
            vrstaUredajaBindingSource.EndEdit();
            vrstaUredajaTableAdapter.Update(ictServisDataSet1.VrstaUredaja);
        }
    }
}
