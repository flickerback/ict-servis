﻿namespace MalinaricJelena_ICTServis
{
    partial class ReportUredajForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportUredajForm));
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.UredajBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ICTServisDataSet = new MalinaricJelena_ICTServis.ICTServisDataSet();
            this.UredajTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.UredajTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.UredajBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ICTServisDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource4.Name = "DataSet1";
            reportDataSource4.Value = this.UredajBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource4);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "MalinaricJelena_ICTServis.Uredaj.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(426, 469);
            this.reportViewer1.TabIndex = 0;
            // 
            // UredajBindingSource
            // 
            this.UredajBindingSource.DataMember = "Uredaj";
            this.UredajBindingSource.DataSource = this.ICTServisDataSet;
            // 
            // ICTServisDataSet
            // 
            this.ICTServisDataSet.DataSetName = "ICTServisDataSet";
            this.ICTServisDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // UredajTableAdapter
            // 
            this.UredajTableAdapter.ClearBeforeFill = true;
            // 
            // ReportUredajForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.ClientSize = new System.Drawing.Size(423, 470);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReportUredajForm";
            this.Text = "Izvještaj - popis uređaja";
            this.Load += new System.EventHandler(this.ReportUredajForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.UredajBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ICTServisDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource UredajBindingSource;
        private ICTServisDataSet ICTServisDataSet;
        private ICTServisDataSetTableAdapters.UredajTableAdapter UredajTableAdapter;
    }
}