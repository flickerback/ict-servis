﻿namespace MalinaricJelena_ICTServis
{
    partial class ZaposleniciForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZaposleniciForm));
            this.cb_radnoMjesto = new System.Windows.Forms.ComboBox();
            this.zaposlenikBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ictServisDataSet1 = new MalinaricJelena_ICTServis.ICTServisDataSet();
            this.radnoMjestoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.osobaIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.OsobaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.radnoMjestoIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.OIB = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Adresa = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.mjestoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsb_new = new System.Windows.Forms.ToolStripButton();
            this.tsb_novaOsoba = new System.Windows.Forms.ToolStripButton();
            this.tsb_spremi = new System.Windows.Forms.ToolStripButton();
            this.tsbObrisi = new System.Windows.Forms.ToolStripButton();
            this.zaposlenikTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.ZaposlenikTableAdapter();
            this.osobaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.OsobaTableAdapter();
            this.radnaMjestaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.RadnaMjestaTableAdapter();
            this.mjestoTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.MjestoTableAdapter();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.zaposlenikBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radnoMjestoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OsobaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mjestoBindingSource)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cb_radnoMjesto
            // 
            this.cb_radnoMjesto.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.zaposlenikBindingSource, "RadnoMjestoID", true));
            this.cb_radnoMjesto.DataSource = this.radnoMjestoBindingSource;
            this.cb_radnoMjesto.DisplayMember = "Naziv";
            this.cb_radnoMjesto.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.cb_radnoMjesto.FormattingEnabled = true;
            this.cb_radnoMjesto.Location = new System.Drawing.Point(254, 355);
            this.cb_radnoMjesto.Name = "cb_radnoMjesto";
            this.cb_radnoMjesto.Size = new System.Drawing.Size(175, 24);
            this.cb_radnoMjesto.TabIndex = 20;
            this.cb_radnoMjesto.ValueMember = "ID";
            // 
            // zaposlenikBindingSource
            // 
            this.zaposlenikBindingSource.DataMember = "Zaposlenik";
            this.zaposlenikBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // ictServisDataSet1
            // 
            this.ictServisDataSet1.DataSetName = "ICTServisDataSet";
            this.ictServisDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // radnoMjestoBindingSource
            // 
            this.radnoMjestoBindingSource.DataMember = "RadnaMjesta";
            this.radnoMjestoBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label1.Location = new System.Drawing.Point(251, 336);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 14;
            this.label1.Text = "Radno mjesto:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.LightCyan;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.osobaIDDataGridViewTextBoxColumn,
            this.radnoMjestoIDDataGridViewTextBoxColumn,
            this.OIB,
            this.Adresa,
            this.Email});
            this.dataGridView1.DataSource = this.zaposlenikBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(671, 286);
            this.dataGridView1.TabIndex = 13;
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // osobaIDDataGridViewTextBoxColumn
            // 
            this.osobaIDDataGridViewTextBoxColumn.DataPropertyName = "OsobaID";
            this.osobaIDDataGridViewTextBoxColumn.DataSource = this.OsobaBindingSource;
            this.osobaIDDataGridViewTextBoxColumn.DisplayMember = "PrezimeIme";
            this.osobaIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.osobaIDDataGridViewTextBoxColumn.HeaderText = "Osoba";
            this.osobaIDDataGridViewTextBoxColumn.Name = "osobaIDDataGridViewTextBoxColumn";
            this.osobaIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.osobaIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.osobaIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.osobaIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // OsobaBindingSource
            // 
            this.OsobaBindingSource.DataMember = "Osoba";
            this.OsobaBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // radnoMjestoIDDataGridViewTextBoxColumn
            // 
            this.radnoMjestoIDDataGridViewTextBoxColumn.DataPropertyName = "RadnoMjestoID";
            this.radnoMjestoIDDataGridViewTextBoxColumn.DataSource = this.radnoMjestoBindingSource;
            this.radnoMjestoIDDataGridViewTextBoxColumn.DisplayMember = "Naziv";
            this.radnoMjestoIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.radnoMjestoIDDataGridViewTextBoxColumn.HeaderText = "Radno mjesto";
            this.radnoMjestoIDDataGridViewTextBoxColumn.Name = "radnoMjestoIDDataGridViewTextBoxColumn";
            this.radnoMjestoIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.radnoMjestoIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.radnoMjestoIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.radnoMjestoIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // OIB
            // 
            this.OIB.DataPropertyName = "OsobaID";
            this.OIB.DataSource = this.OsobaBindingSource;
            this.OIB.DisplayMember = "OIB";
            this.OIB.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.OIB.HeaderText = "OIB";
            this.OIB.Name = "OIB";
            this.OIB.ReadOnly = true;
            this.OIB.ValueMember = "ID";
            // 
            // Adresa
            // 
            this.Adresa.DataPropertyName = "OsobaID";
            this.Adresa.DataSource = this.OsobaBindingSource;
            this.Adresa.DisplayMember = "Adresa";
            this.Adresa.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.Adresa.HeaderText = "Adresa";
            this.Adresa.Name = "Adresa";
            this.Adresa.ReadOnly = true;
            this.Adresa.ValueMember = "ID";
            // 
            // Email
            // 
            this.Email.DataPropertyName = "OsobaID";
            this.Email.DataSource = this.OsobaBindingSource;
            this.Email.DisplayMember = "Email";
            this.Email.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.Email.HeaderText = "Email";
            this.Email.Name = "Email";
            this.Email.ReadOnly = true;
            this.Email.ValueMember = "ID";
            // 
            // mjestoBindingSource
            // 
            this.mjestoBindingSource.DataMember = "Mjesto";
            this.mjestoBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsb_new,
            this.tsb_novaOsoba,
            this.tsb_spremi,
            this.tsbObrisi});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(671, 25);
            this.toolStrip1.TabIndex = 12;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsb_new
            // 
            this.tsb_new.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tsb_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_new.Image = global::MalinaricJelena_ICTServis.Properties.Resources._new;
            this.tsb_new.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_new.Name = "tsb_new";
            this.tsb_new.Size = new System.Drawing.Size(118, 22);
            this.tsb_new.Text = "Novi zaposlenik";
            this.tsb_new.Click += new System.EventHandler(this.tsb_new_Click);
            // 
            // tsb_novaOsoba
            // 
            this.tsb_novaOsoba.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tsb_novaOsoba.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_novaOsoba.Image = global::MalinaricJelena_ICTServis.Properties.Resources.user;
            this.tsb_novaOsoba.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_novaOsoba.Name = "tsb_novaOsoba";
            this.tsb_novaOsoba.Size = new System.Drawing.Size(95, 22);
            this.tsb_novaOsoba.Text = "Nova osoba";
            this.tsb_novaOsoba.Click += new System.EventHandler(this.tsb_novaOsoba_Click);
            // 
            // tsb_spremi
            // 
            this.tsb_spremi.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.tsb_spremi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_spremi.Image = global::MalinaricJelena_ICTServis.Properties.Resources.save;
            this.tsb_spremi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_spremi.Name = "tsb_spremi";
            this.tsb_spremi.Size = new System.Drawing.Size(70, 22);
            this.tsb_spremi.Text = "Spremi";
            this.tsb_spremi.Click += new System.EventHandler(this.tsb_spremi_Click);
            // 
            // tsbObrisi
            // 
            this.tsbObrisi.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.tsbObrisi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsbObrisi.Image = global::MalinaricJelena_ICTServis.Properties.Resources.x_button;
            this.tsbObrisi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbObrisi.Name = "tsbObrisi";
            this.tsbObrisi.Size = new System.Drawing.Size(62, 22);
            this.tsbObrisi.Text = "Obriši";
            this.tsbObrisi.Click += new System.EventHandler(this.tsbObrisi_Click);
            // 
            // zaposlenikTableAdapter
            // 
            this.zaposlenikTableAdapter.ClearBeforeFill = true;
            // 
            // osobaTableAdapter
            // 
            this.osobaTableAdapter.ClearBeforeFill = true;
            // 
            // radnaMjestaTableAdapter
            // 
            this.radnaMjestaTableAdapter.ClearBeforeFill = true;
            // 
            // mjestoTableAdapter
            // 
            this.mjestoTableAdapter.ClearBeforeFill = true;
            // 
            // comboBox1
            // 
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.zaposlenikBindingSource, "OsobaID", true));
            this.comboBox1.DataSource = this.OsobaBindingSource;
            this.comboBox1.DisplayMember = "PrezimeIme";
            this.comboBox1.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(25, 355);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(175, 24);
            this.comboBox1.TabIndex = 22;
            this.comboBox1.ValueMember = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label2.Location = new System.Drawing.Point(22, 336);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 16);
            this.label2.TabIndex = 21;
            this.label2.Text = "Osoba:";
            // 
            // ZaposleniciForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MalinaricJelena_ICTServis.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(671, 411);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cb_radnoMjesto);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ZaposleniciForm";
            this.Text = "Zaposlenici";
            this.Load += new System.EventHandler(this.ZaposleniciForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.zaposlenikBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radnoMjestoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OsobaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mjestoBindingSource)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cb_radnoMjesto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsb_new;
        private System.Windows.Forms.ToolStripButton tsb_spremi;
        private ICTServisDataSet ictServisDataSet1;
        private System.Windows.Forms.BindingSource zaposlenikBindingSource;
        private ICTServisDataSetTableAdapters.ZaposlenikTableAdapter zaposlenikTableAdapter;
        private System.Windows.Forms.BindingSource OsobaBindingSource;
        private System.Windows.Forms.BindingSource radnoMjestoBindingSource;
        private System.Windows.Forms.BindingSource mjestoBindingSource;
        private ICTServisDataSetTableAdapters.OsobaTableAdapter osobaTableAdapter;
        private ICTServisDataSetTableAdapters.RadnaMjestaTableAdapter radnaMjestaTableAdapter;
        private ICTServisDataSetTableAdapters.MjestoTableAdapter mjestoTableAdapter;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripButton tsb_novaOsoba;
        private System.Windows.Forms.DataGridViewComboBoxColumn osobaIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn radnoMjestoIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn OIB;
        private System.Windows.Forms.DataGridViewComboBoxColumn Adresa;
        private System.Windows.Forms.DataGridViewComboBoxColumn Email;
        private System.Windows.Forms.ToolStripButton tsbObrisi;
    }
}