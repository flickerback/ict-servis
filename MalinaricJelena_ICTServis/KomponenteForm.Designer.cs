﻿namespace MalinaricJelena_ICTServis
{
    partial class KomponenteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KomponenteForm));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vrstaIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.vrstaKomponenteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ictServisDataSet1 = new MalinaricJelena_ICTServis.ICTServisDataSet();
            this.modelKomponenteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.proizvodacIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.proizvodacBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nazivDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cijenaKomponenteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.komponentaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsb_new = new System.Windows.Forms.ToolStripButton();
            this.tsb_spremi = new System.Windows.Forms.ToolStripButton();
            this.cb_Proizvodac = new System.Windows.Forms.ComboBox();
            this.cb_tipKomponente = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNaziv = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_model = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCijena = new System.Windows.Forms.TextBox();
            this.vrstaKomponenteTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.VrstaKomponenteTableAdapter();
            this.proizvodacTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.ProizvodacTableAdapter();
            this.komponentaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.KomponentaTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vrstaKomponenteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.proizvodacBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.komponentaBindingSource)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.Lavender;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.vrstaIDDataGridViewTextBoxColumn,
            this.modelKomponenteDataGridViewTextBoxColumn,
            this.proizvodacIDDataGridViewTextBoxColumn,
            this.nazivDataGridViewTextBoxColumn,
            this.cijenaKomponenteDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.komponentaBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(545, 291);
            this.dataGridView1.TabIndex = 0;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.iDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // vrstaIDDataGridViewTextBoxColumn
            // 
            this.vrstaIDDataGridViewTextBoxColumn.DataPropertyName = "VrstaID";
            this.vrstaIDDataGridViewTextBoxColumn.DataSource = this.vrstaKomponenteBindingSource;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.vrstaIDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.vrstaIDDataGridViewTextBoxColumn.DisplayMember = "Naziv";
            this.vrstaIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.vrstaIDDataGridViewTextBoxColumn.HeaderText = "Tip komponente";
            this.vrstaIDDataGridViewTextBoxColumn.Name = "vrstaIDDataGridViewTextBoxColumn";
            this.vrstaIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.vrstaIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.vrstaIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.vrstaIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // vrstaKomponenteBindingSource
            // 
            this.vrstaKomponenteBindingSource.DataMember = "VrstaKomponente";
            this.vrstaKomponenteBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // ictServisDataSet1
            // 
            this.ictServisDataSet1.DataSetName = "ICTServisDataSet";
            this.ictServisDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // modelKomponenteDataGridViewTextBoxColumn
            // 
            this.modelKomponenteDataGridViewTextBoxColumn.DataPropertyName = "ModelKomponente";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.modelKomponenteDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.modelKomponenteDataGridViewTextBoxColumn.HeaderText = "Model komponente";
            this.modelKomponenteDataGridViewTextBoxColumn.Name = "modelKomponenteDataGridViewTextBoxColumn";
            this.modelKomponenteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // proizvodacIDDataGridViewTextBoxColumn
            // 
            this.proizvodacIDDataGridViewTextBoxColumn.DataPropertyName = "ProizvodacID";
            this.proizvodacIDDataGridViewTextBoxColumn.DataSource = this.proizvodacBindingSource;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.proizvodacIDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.proizvodacIDDataGridViewTextBoxColumn.DisplayMember = "Naziv";
            this.proizvodacIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.proizvodacIDDataGridViewTextBoxColumn.HeaderText = "Proizvođač";
            this.proizvodacIDDataGridViewTextBoxColumn.Name = "proizvodacIDDataGridViewTextBoxColumn";
            this.proizvodacIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.proizvodacIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.proizvodacIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.proizvodacIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // proizvodacBindingSource
            // 
            this.proizvodacBindingSource.DataMember = "Proizvodac";
            this.proizvodacBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // nazivDataGridViewTextBoxColumn
            // 
            this.nazivDataGridViewTextBoxColumn.DataPropertyName = "Naziv";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.nazivDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.nazivDataGridViewTextBoxColumn.HeaderText = "Naziv";
            this.nazivDataGridViewTextBoxColumn.Name = "nazivDataGridViewTextBoxColumn";
            this.nazivDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cijenaKomponenteDataGridViewTextBoxColumn
            // 
            this.cijenaKomponenteDataGridViewTextBoxColumn.DataPropertyName = "CijenaKomponente";
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle7.Format = "C2";
            dataGridViewCellStyle7.NullValue = "-";
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cijenaKomponenteDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.cijenaKomponenteDataGridViewTextBoxColumn.HeaderText = "Cijena";
            this.cijenaKomponenteDataGridViewTextBoxColumn.Name = "cijenaKomponenteDataGridViewTextBoxColumn";
            this.cijenaKomponenteDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // komponentaBindingSource
            // 
            this.komponentaBindingSource.DataMember = "Komponenta";
            this.komponentaBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsb_new,
            this.tsb_spremi});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(545, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsb_new
            // 
            this.tsb_new.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.tsb_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_new.Image = global::MalinaricJelena_ICTServis.Properties.Resources._new;
            this.tsb_new.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_new.Name = "tsb_new";
            this.tsb_new.Size = new System.Drawing.Size(54, 22);
            this.tsb_new.Text = "Novi";
            this.tsb_new.Click += new System.EventHandler(this.tsb_new_Click);
            // 
            // tsb_spremi
            // 
            this.tsb_spremi.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tsb_spremi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_spremi.Image = global::MalinaricJelena_ICTServis.Properties.Resources.save;
            this.tsb_spremi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_spremi.Name = "tsb_spremi";
            this.tsb_spremi.Size = new System.Drawing.Size(70, 22);
            this.tsb_spremi.Text = "Spremi";
            this.tsb_spremi.Click += new System.EventHandler(this.tsb_spremi_Click);
            // 
            // cb_Proizvodac
            // 
            this.cb_Proizvodac.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.komponentaBindingSource, "ProizvodacID", true));
            this.cb_Proizvodac.DataSource = this.proizvodacBindingSource;
            this.cb_Proizvodac.DisplayMember = "Naziv";
            this.cb_Proizvodac.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.cb_Proizvodac.FormattingEnabled = true;
            this.cb_Proizvodac.Location = new System.Drawing.Point(318, 362);
            this.cb_Proizvodac.Name = "cb_Proizvodac";
            this.cb_Proizvodac.Size = new System.Drawing.Size(175, 24);
            this.cb_Proizvodac.TabIndex = 19;
            this.cb_Proizvodac.ValueMember = "ID";
            // 
            // cb_tipKomponente
            // 
            this.cb_tipKomponente.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.komponentaBindingSource, "VrstaID", true));
            this.cb_tipKomponente.DataSource = this.vrstaKomponenteBindingSource;
            this.cb_tipKomponente.DisplayMember = "Naziv";
            this.cb_tipKomponente.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_tipKomponente.FormattingEnabled = true;
            this.cb_tipKomponente.Location = new System.Drawing.Point(22, 362);
            this.cb_tipKomponente.Name = "cb_tipKomponente";
            this.cb_tipKomponente.Size = new System.Drawing.Size(175, 24);
            this.cb_tipKomponente.TabIndex = 18;
            this.cb_tipKomponente.ValueMember = "ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label3.Location = new System.Drawing.Point(319, 419);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "Naziv:";
            // 
            // txtNaziv
            // 
            this.txtNaziv.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.komponentaBindingSource, "Naziv", true));
            this.txtNaziv.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.txtNaziv.Location = new System.Drawing.Point(318, 438);
            this.txtNaziv.Name = "txtNaziv";
            this.txtNaziv.Size = new System.Drawing.Size(175, 23);
            this.txtNaziv.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label4.Location = new System.Drawing.Point(315, 343);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 16);
            this.label4.TabIndex = 15;
            this.label4.Text = "Proizvođač: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label2.Location = new System.Drawing.Point(20, 419);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 16);
            this.label2.TabIndex = 14;
            this.label2.Text = "Model komponente:";
            // 
            // txt_model
            // 
            this.txt_model.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.komponentaBindingSource, "ModelKomponente", true));
            this.txt_model.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.txt_model.Location = new System.Drawing.Point(22, 438);
            this.txt_model.Multiline = true;
            this.txt_model.Name = "txt_model";
            this.txt_model.Size = new System.Drawing.Size(175, 76);
            this.txt_model.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(20, 343);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 12;
            this.label1.Text = "Tip komponente:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label5.Location = new System.Drawing.Point(319, 475);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 16);
            this.label5.TabIndex = 21;
            this.label5.Text = "Cijena:";
            // 
            // txtCijena
            // 
            this.txtCijena.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.komponentaBindingSource, "CijenaKomponente", true));
            this.txtCijena.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.txtCijena.Location = new System.Drawing.Point(318, 494);
            this.txtCijena.Name = "txtCijena";
            this.txtCijena.Size = new System.Drawing.Size(175, 23);
            this.txtCijena.TabIndex = 20;
            // 
            // vrstaKomponenteTableAdapter
            // 
            this.vrstaKomponenteTableAdapter.ClearBeforeFill = true;
            // 
            // proizvodacTableAdapter
            // 
            this.proizvodacTableAdapter.ClearBeforeFill = true;
            // 
            // komponentaTableAdapter
            // 
            this.komponentaTableAdapter.ClearBeforeFill = true;
            // 
            // KomponenteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MalinaricJelena_ICTServis.Properties.Resources.background_purple;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(545, 541);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCijena);
            this.Controls.Add(this.cb_Proizvodac);
            this.Controls.Add(this.cb_tipKomponente);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNaziv);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_model);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "KomponenteForm";
            this.Text = "Komponente";
            this.Load += new System.EventHandler(this.KomponenteForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vrstaKomponenteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.proizvodacBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.komponentaBindingSource)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsb_new;
        private System.Windows.Forms.ToolStripButton tsb_spremi;
        private System.Windows.Forms.ComboBox cb_Proizvodac;
        private System.Windows.Forms.ComboBox cb_tipKomponente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNaziv;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_model;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCijena;
        private ICTServisDataSet ictServisDataSet1;
        private System.Windows.Forms.BindingSource vrstaKomponenteBindingSource;
        private ICTServisDataSetTableAdapters.VrstaKomponenteTableAdapter vrstaKomponenteTableAdapter;
        private System.Windows.Forms.BindingSource proizvodacBindingSource;
        private ICTServisDataSetTableAdapters.ProizvodacTableAdapter proizvodacTableAdapter;
        private System.Windows.Forms.BindingSource komponentaBindingSource;
        private ICTServisDataSetTableAdapters.KomponentaTableAdapter komponentaTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn vrstaIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelKomponenteDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn proizvodacIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazivDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cijenaKomponenteDataGridViewTextBoxColumn;
    }
}