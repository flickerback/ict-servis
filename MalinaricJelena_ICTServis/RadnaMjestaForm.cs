﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class RadnaMjestaForm : Form
    {
        public RadnaMjestaForm()
        {
            InitializeComponent();
            radnaMjestaTableAdapter.Adapter.RowUpdated += Adapter_RowUpdated;
        }
    
        void Adapter_RowUpdated(object sender, System.Data.OleDb.OleDbRowUpdatedEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {
                var cmd = new OleDbCommand("SELECT @@IDENTITY", e.Command.Connection);
                var id = (int)cmd.ExecuteScalar();
                e.Row["ID"] = id;
                e.Row.AcceptChanges();
            }
        }

        private void RadnaMjestaForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ictServisDataSet1.RadnaMjesta' table. You can move, or remove it, as needed.
            this.radnaMjestaTableAdapter.Fill(this.ictServisDataSet1.RadnaMjesta);

        }

        private void tsb_save_Click(object sender, EventArgs e)
        {
            radnaMjestaBindingSource.EndEdit();
            radnaMjestaTableAdapter.Update(ictServisDataSet1.RadnaMjesta);
        }
    }
}
