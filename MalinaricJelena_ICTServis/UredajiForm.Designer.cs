﻿namespace MalinaricJelena_ICTServis
{
    partial class UredajForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UredajForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsb_new = new System.Windows.Forms.ToolStripButton();
            this.tsb_spremi = new System.Windows.Forms.ToolStripButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.vrstaUredajaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ictServisDataSet1 = new MalinaricJelena_ICTServis.ICTServisDataSet();
            this.proizvodacBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.uredajBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.vrstaUredajaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.VrstaUredajaTableAdapter();
            this.proizvodacTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.ProizvodacTableAdapter();
            this.uredajTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.UredajTableAdapter();
            this.cb_tipUredaja = new System.Windows.Forms.ComboBox();
            this.cb_Proizvodac = new System.Windows.Forms.ComboBox();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vrstaUredajaIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.modelUredajaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.proizvodacIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.nazivDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vrstaUredajaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.proizvodacBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uredajBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsb_new,
            this.tsb_spremi});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(532, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsb_new
            // 
            this.tsb_new.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tsb_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_new.Image = global::MalinaricJelena_ICTServis.Properties.Resources._new;
            this.tsb_new.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_new.Name = "tsb_new";
            this.tsb_new.Size = new System.Drawing.Size(54, 22);
            this.tsb_new.Text = "Novi";
            this.tsb_new.Click += new System.EventHandler(this.tsb_new_Click);
            // 
            // tsb_spremi
            // 
            this.tsb_spremi.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.tsb_spremi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_spremi.Image = global::MalinaricJelena_ICTServis.Properties.Resources.save;
            this.tsb_spremi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_spremi.Name = "tsb_spremi";
            this.tsb_spremi.Size = new System.Drawing.Size(70, 22);
            this.tsb_spremi.Text = "Spremi";
            this.tsb_spremi.Click += new System.EventHandler(this.tsb_spremi_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.Lavender;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.vrstaUredajaIDDataGridViewTextBoxColumn,
            this.modelUredajaDataGridViewTextBoxColumn,
            this.proizvodacIDDataGridViewTextBoxColumn,
            this.nazivDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.uredajBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(532, 291);
            this.dataGridView1.TabIndex = 1;
            // 
            // vrstaUredajaBindingSource
            // 
            this.vrstaUredajaBindingSource.DataMember = "VrstaUredaja";
            this.vrstaUredajaBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // ictServisDataSet1
            // 
            this.ictServisDataSet1.DataSetName = "ICTServisDataSet";
            this.ictServisDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // proizvodacBindingSource
            // 
            this.proizvodacBindingSource.DataMember = "Proizvodac";
            this.proizvodacBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // uredajBindingSource
            // 
            this.uredajBindingSource.DataMember = "Uredaj";
            this.uredajBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label1.Location = new System.Drawing.Point(24, 342);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Tip uređaja:";
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.uredajBindingSource, "ModelUredaja", true));
            this.textBox1.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.textBox1.Location = new System.Drawing.Point(27, 437);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(175, 76);
            this.textBox1.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label2.Location = new System.Drawing.Point(24, 418);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Model uređaja:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label3.Location = new System.Drawing.Point(320, 418);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Naziv:";
            // 
            // textBox2
            // 
            this.textBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.uredajBindingSource, "Naziv", true));
            this.textBox2.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.textBox2.Location = new System.Drawing.Point(323, 437);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(175, 23);
            this.textBox2.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label4.Location = new System.Drawing.Point(320, 342);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Proizvođač: ";
            // 
            // vrstaUredajaTableAdapter
            // 
            this.vrstaUredajaTableAdapter.ClearBeforeFill = true;
            // 
            // proizvodacTableAdapter
            // 
            this.proizvodacTableAdapter.ClearBeforeFill = true;
            // 
            // uredajTableAdapter
            // 
            this.uredajTableAdapter.ClearBeforeFill = true;
            // 
            // cb_tipUredaja
            // 
            this.cb_tipUredaja.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.uredajBindingSource, "VrstaUredajaID", true));
            this.cb_tipUredaja.DataSource = this.vrstaUredajaBindingSource;
            this.cb_tipUredaja.DisplayMember = "Naziv";
            this.cb_tipUredaja.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.cb_tipUredaja.FormattingEnabled = true;
            this.cb_tipUredaja.Location = new System.Drawing.Point(27, 361);
            this.cb_tipUredaja.Name = "cb_tipUredaja";
            this.cb_tipUredaja.Size = new System.Drawing.Size(175, 24);
            this.cb_tipUredaja.TabIndex = 10;
            this.cb_tipUredaja.ValueMember = "ID";
            // 
            // cb_Proizvodac
            // 
            this.cb_Proizvodac.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.uredajBindingSource, "ProizvodacID", true));
            this.cb_Proizvodac.DataSource = this.proizvodacBindingSource;
            this.cb_Proizvodac.DisplayMember = "Naziv";
            this.cb_Proizvodac.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.cb_Proizvodac.FormattingEnabled = true;
            this.cb_Proizvodac.Location = new System.Drawing.Point(323, 361);
            this.cb_Proizvodac.Name = "cb_Proizvodac";
            this.cb_Proizvodac.Size = new System.Drawing.Size(175, 24);
            this.cb_Proizvodac.TabIndex = 11;
            this.cb_Proizvodac.ValueMember = "ID";
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.iDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // vrstaUredajaIDDataGridViewTextBoxColumn
            // 
            this.vrstaUredajaIDDataGridViewTextBoxColumn.DataPropertyName = "VrstaUredajaID";
            this.vrstaUredajaIDDataGridViewTextBoxColumn.DataSource = this.vrstaUredajaBindingSource;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.vrstaUredajaIDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.vrstaUredajaIDDataGridViewTextBoxColumn.DisplayMember = "Naziv";
            this.vrstaUredajaIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.vrstaUredajaIDDataGridViewTextBoxColumn.HeaderText = "Tip uređaja";
            this.vrstaUredajaIDDataGridViewTextBoxColumn.Name = "vrstaUredajaIDDataGridViewTextBoxColumn";
            this.vrstaUredajaIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.vrstaUredajaIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.vrstaUredajaIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.vrstaUredajaIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // modelUredajaDataGridViewTextBoxColumn
            // 
            this.modelUredajaDataGridViewTextBoxColumn.DataPropertyName = "ModelUredaja";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.modelUredajaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.modelUredajaDataGridViewTextBoxColumn.HeaderText = "Model uređaja";
            this.modelUredajaDataGridViewTextBoxColumn.Name = "modelUredajaDataGridViewTextBoxColumn";
            this.modelUredajaDataGridViewTextBoxColumn.ReadOnly = true;
            this.modelUredajaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // proizvodacIDDataGridViewTextBoxColumn
            // 
            this.proizvodacIDDataGridViewTextBoxColumn.DataPropertyName = "ProizvodacID";
            this.proizvodacIDDataGridViewTextBoxColumn.DataSource = this.proizvodacBindingSource;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.proizvodacIDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.proizvodacIDDataGridViewTextBoxColumn.DisplayMember = "Naziv";
            this.proizvodacIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.proizvodacIDDataGridViewTextBoxColumn.HeaderText = "Proizvođač";
            this.proizvodacIDDataGridViewTextBoxColumn.Name = "proizvodacIDDataGridViewTextBoxColumn";
            this.proizvodacIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.proizvodacIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.proizvodacIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.proizvodacIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // nazivDataGridViewTextBoxColumn
            // 
            this.nazivDataGridViewTextBoxColumn.DataPropertyName = "Naziv";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.nazivDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.nazivDataGridViewTextBoxColumn.HeaderText = "Naziv";
            this.nazivDataGridViewTextBoxColumn.Name = "nazivDataGridViewTextBoxColumn";
            this.nazivDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // UredajForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MalinaricJelena_ICTServis.Properties.Resources.background_purple;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(532, 541);
            this.Controls.Add(this.cb_Proizvodac);
            this.Controls.Add(this.cb_tipUredaja);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UredajForm";
            this.Text = "Uređaji";
            this.Load += new System.EventHandler(this.UredajForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vrstaUredajaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.proizvodacBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uredajBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripButton tsb_new;
        private System.Windows.Forms.ToolStripButton tsb_spremi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label4;
        private ICTServisDataSet ictServisDataSet1;
        private System.Windows.Forms.BindingSource vrstaUredajaBindingSource;
        private ICTServisDataSetTableAdapters.VrstaUredajaTableAdapter vrstaUredajaTableAdapter;
        private System.Windows.Forms.BindingSource proizvodacBindingSource;
        private ICTServisDataSetTableAdapters.ProizvodacTableAdapter proizvodacTableAdapter;
        private System.Windows.Forms.BindingSource uredajBindingSource;
        private ICTServisDataSetTableAdapters.UredajTableAdapter uredajTableAdapter;
        private System.Windows.Forms.ComboBox cb_tipUredaja;
        private System.Windows.Forms.ComboBox cb_Proizvodac;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn vrstaUredajaIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelUredajaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn proizvodacIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazivDataGridViewTextBoxColumn;
    }
}