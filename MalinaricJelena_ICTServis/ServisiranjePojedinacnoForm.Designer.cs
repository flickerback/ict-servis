﻿namespace MalinaricJelena_ICTServis
{
    partial class ServisiranjePojedinacnoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServisiranjePojedinacnoForm));
            this.dtp_datumNarudzbe = new System.Windows.Forms.DateTimePicker();
            this.servisiranjeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ictServisDataSet1 = new MalinaricJelena_ICTServis.ICTServisDataSet();
            this.servisiranjeServisiranjeKomponentaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cb_komponenta = new System.Windows.Forms.ComboBox();
            this.osobaKlijentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cb_narucitelj = new System.Windows.Forms.ComboBox();
            this.osobaZaposlenikBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsb_spremi = new System.Windows.Forms.ToolStripButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.komponentaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.uslugaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.uredajBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.servisiranjeTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.ServisiranjeTableAdapter();
            this.servisiranjeKomponentaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.ServisiranjeKomponentaTableAdapter();
            this.komponentaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.KomponentaTableAdapter();
            this.osobaZaposlenikTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.OsobaZaposlenikTableAdapter();
            this.uredajTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.UredajTableAdapter();
            this.uslugaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.UslugaTableAdapter();
            this.osobaKlijentTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.OsobaKlijentTableAdapter();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.iDServisiranjeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDKomponentaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.potrebnaKolicinaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.servisiranjeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servisiranjeServisiranjeKomponentaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osobaKlijentBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osobaZaposlenikBindingSource)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.komponentaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uslugaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uredajBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dtp_datumNarudzbe
            // 
            this.dtp_datumNarudzbe.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.servisiranjeBindingSource, "DatumZavrsetka", true));
            this.dtp_datumNarudzbe.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtp_datumNarudzbe.Location = new System.Drawing.Point(417, 139);
            this.dtp_datumNarudzbe.Name = "dtp_datumNarudzbe";
            this.dtp_datumNarudzbe.Size = new System.Drawing.Size(175, 23);
            this.dtp_datumNarudzbe.TabIndex = 44;
            // 
            // servisiranjeBindingSource
            // 
            this.servisiranjeBindingSource.DataMember = "Servisiranje";
            this.servisiranjeBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // ictServisDataSet1
            // 
            this.ictServisDataSet1.DataSetName = "ICTServisDataSet";
            this.ictServisDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // servisiranjeServisiranjeKomponentaBindingSource
            // 
            this.servisiranjeServisiranjeKomponentaBindingSource.DataMember = "ServisiranjeServisiranjeKomponenta";
            this.servisiranjeServisiranjeKomponentaBindingSource.DataSource = this.servisiranjeBindingSource;
            // 
            // cb_komponenta
            // 
            this.cb_komponenta.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.servisiranjeBindingSource, "KlijentID", true));
            this.cb_komponenta.DataSource = this.osobaKlijentBindingSource;
            this.cb_komponenta.DisplayMember = "PrezimeIme";
            this.cb_komponenta.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.cb_komponenta.FormattingEnabled = true;
            this.cb_komponenta.Location = new System.Drawing.Point(15, 138);
            this.cb_komponenta.Name = "cb_komponenta";
            this.cb_komponenta.Size = new System.Drawing.Size(175, 24);
            this.cb_komponenta.TabIndex = 41;
            this.cb_komponenta.ValueMember = "ID";
            // 
            // osobaKlijentBindingSource
            // 
            this.osobaKlijentBindingSource.DataMember = "OsobaKlijent";
            this.osobaKlijentBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // cb_narucitelj
            // 
            this.cb_narucitelj.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.servisiranjeBindingSource, "ServiserID", true));
            this.cb_narucitelj.DataSource = this.osobaZaposlenikBindingSource;
            this.cb_narucitelj.DisplayMember = "PrezimeIme";
            this.cb_narucitelj.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_narucitelj.FormattingEnabled = true;
            this.cb_narucitelj.Location = new System.Drawing.Point(14, 64);
            this.cb_narucitelj.Name = "cb_narucitelj";
            this.cb_narucitelj.Size = new System.Drawing.Size(175, 24);
            this.cb_narucitelj.TabIndex = 40;
            this.cb_narucitelj.ValueMember = "ID";
            // 
            // osobaZaposlenikBindingSource
            // 
            this.osobaZaposlenikBindingSource.DataMember = "OsobaZaposlenik";
            this.osobaZaposlenikBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label3.Location = new System.Drawing.Point(414, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 16);
            this.label3.TabIndex = 39;
            this.label3.Text = "Datum završetka:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label4.Location = new System.Drawing.Point(12, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 16);
            this.label4.TabIndex = 38;
            this.label4.Text = "Klijent:";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsb_spremi});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(621, 25);
            this.toolStrip1.TabIndex = 36;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsb_spremi
            // 
            this.tsb_spremi.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tsb_spremi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_spremi.Image = global::MalinaricJelena_ICTServis.Properties.Resources.save;
            this.tsb_spremi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_spremi.Name = "tsb_spremi";
            this.tsb_spremi.Size = new System.Drawing.Size(70, 22);
            this.tsb_spremi.Text = "Spremi";
            this.tsb_spremi.Click += new System.EventHandler(this.tsb_spremi_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.MistyRose;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDServisiranjeDataGridViewTextBoxColumn,
            this.iDKomponentaDataGridViewTextBoxColumn,
            this.potrebnaKolicinaDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.servisiranjeServisiranjeKomponentaBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 280);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(621, 225);
            this.dataGridView1.TabIndex = 35;
            this.dataGridView1.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridView1_DataError);
            // 
            // komponentaBindingSource
            // 
            this.komponentaBindingSource.DataMember = "Komponenta";
            this.komponentaBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 16);
            this.label1.TabIndex = 37;
            this.label1.Text = "Serviser:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label2.Location = new System.Drawing.Point(11, 258);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 16);
            this.label2.TabIndex = 46;
            this.label2.Text = "Potrebne komponente:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.servisiranjeBindingSource, "DatumPredaje", true));
            this.dateTimePicker1.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dateTimePicker1.Location = new System.Drawing.Point(417, 65);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(175, 23);
            this.dateTimePicker1.TabIndex = 48;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label6.Location = new System.Drawing.Point(414, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 16);
            this.label6.TabIndex = 47;
            this.label6.Text = "Datum predaje:";
            // 
            // comboBox1
            // 
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.servisiranjeBindingSource, "UslugaID", true));
            this.comboBox1.DataSource = this.uslugaBindingSource;
            this.comboBox1.DisplayMember = "Naziv";
            this.comboBox1.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(217, 138);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(175, 24);
            this.comboBox1.TabIndex = 52;
            this.comboBox1.ValueMember = "ID";
            // 
            // uslugaBindingSource
            // 
            this.uslugaBindingSource.DataMember = "Usluga";
            this.uslugaBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // comboBox2
            // 
            this.comboBox2.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.servisiranjeBindingSource, "UredajID", true));
            this.comboBox2.DataSource = this.uredajBindingSource;
            this.comboBox2.DisplayMember = "Naziv";
            this.comboBox2.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(216, 64);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(175, 24);
            this.comboBox2.TabIndex = 51;
            this.comboBox2.ValueMember = "ID";
            // 
            // uredajBindingSource
            // 
            this.uredajBindingSource.DataMember = "Uredaj";
            this.uredajBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label7.Location = new System.Drawing.Point(214, 119);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 16);
            this.label7.TabIndex = 50;
            this.label7.Text = "Usluga:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(214, 45);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 16);
            this.label8.TabIndex = 49;
            this.label8.Text = "Uređaj:";
            // 
            // servisiranjeTableAdapter
            // 
            this.servisiranjeTableAdapter.ClearBeforeFill = true;
            // 
            // servisiranjeKomponentaTableAdapter
            // 
            this.servisiranjeKomponentaTableAdapter.ClearBeforeFill = true;
            // 
            // komponentaTableAdapter
            // 
            this.komponentaTableAdapter.ClearBeforeFill = true;
            // 
            // osobaZaposlenikTableAdapter
            // 
            this.osobaZaposlenikTableAdapter.ClearBeforeFill = true;
            // 
            // uredajTableAdapter
            // 
            this.uredajTableAdapter.ClearBeforeFill = true;
            // 
            // uslugaTableAdapter
            // 
            this.uslugaTableAdapter.ClearBeforeFill = true;
            // 
            // osobaKlijentTableAdapter
            // 
            this.osobaKlijentTableAdapter.ClearBeforeFill = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.servisiranjeBindingSource, "Pokupljeno", true));
            this.checkBox1.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.checkBox1.Location = new System.Drawing.Point(502, 168);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(90, 20);
            this.checkBox1.TabIndex = 54;
            this.checkBox1.Text = "Pokupljeno";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // iDServisiranjeDataGridViewTextBoxColumn
            // 
            this.iDServisiranjeDataGridViewTextBoxColumn.DataPropertyName = "IDServisiranje";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.iDServisiranjeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.iDServisiranjeDataGridViewTextBoxColumn.HeaderText = "IDServisiranje";
            this.iDServisiranjeDataGridViewTextBoxColumn.Name = "iDServisiranjeDataGridViewTextBoxColumn";
            this.iDServisiranjeDataGridViewTextBoxColumn.Visible = false;
            // 
            // iDKomponentaDataGridViewTextBoxColumn
            // 
            this.iDKomponentaDataGridViewTextBoxColumn.DataPropertyName = "IDKomponenta";
            this.iDKomponentaDataGridViewTextBoxColumn.DataSource = this.komponentaBindingSource;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.iDKomponentaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.iDKomponentaDataGridViewTextBoxColumn.DisplayMember = "Naziv";
            this.iDKomponentaDataGridViewTextBoxColumn.HeaderText = "Komponenta";
            this.iDKomponentaDataGridViewTextBoxColumn.Name = "iDKomponentaDataGridViewTextBoxColumn";
            this.iDKomponentaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.iDKomponentaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.iDKomponentaDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // potrebnaKolicinaDataGridViewTextBoxColumn
            // 
            this.potrebnaKolicinaDataGridViewTextBoxColumn.DataPropertyName = "PotrebnaKolicina";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.potrebnaKolicinaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.potrebnaKolicinaDataGridViewTextBoxColumn.HeaderText = "Količina";
            this.potrebnaKolicinaDataGridViewTextBoxColumn.Name = "potrebnaKolicinaDataGridViewTextBoxColumn";
            // 
            // ServisiranjePojedinacnoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MalinaricJelena_ICTServis.Properties.Resources.background_red;
            this.ClientSize = new System.Drawing.Size(621, 508);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtp_datumNarudzbe);
            this.Controls.Add(this.cb_komponenta);
            this.Controls.Add(this.cb_narucitelj);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ServisiranjePojedinacnoForm";
            this.Text = "Unos servisiranja";
            this.Load += new System.EventHandler(this.ServisiranjePojedinacnoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.servisiranjeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servisiranjeServisiranjeKomponentaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osobaKlijentBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osobaZaposlenikBindingSource)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.komponentaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uslugaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uredajBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtp_datumNarudzbe;
        private System.Windows.Forms.ComboBox cb_komponenta;
        private System.Windows.Forms.ComboBox cb_narucitelj;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsb_spremi;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private ICTServisDataSet ictServisDataSet1;
        private System.Windows.Forms.BindingSource servisiranjeBindingSource;
        private ICTServisDataSetTableAdapters.ServisiranjeTableAdapter servisiranjeTableAdapter;
        private System.Windows.Forms.BindingSource servisiranjeServisiranjeKomponentaBindingSource;
        private ICTServisDataSetTableAdapters.ServisiranjeKomponentaTableAdapter servisiranjeKomponentaTableAdapter;
        private System.Windows.Forms.BindingSource komponentaBindingSource;
        private ICTServisDataSetTableAdapters.KomponentaTableAdapter komponentaTableAdapter;
        private System.Windows.Forms.BindingSource osobaZaposlenikBindingSource;
        private ICTServisDataSetTableAdapters.OsobaZaposlenikTableAdapter osobaZaposlenikTableAdapter;
        private System.Windows.Forms.BindingSource uredajBindingSource;
        private ICTServisDataSetTableAdapters.UredajTableAdapter uredajTableAdapter;
        private System.Windows.Forms.BindingSource uslugaBindingSource;
        private ICTServisDataSetTableAdapters.UslugaTableAdapter uslugaTableAdapter;
        private System.Windows.Forms.BindingSource osobaKlijentBindingSource;
        private ICTServisDataSetTableAdapters.OsobaKlijentTableAdapter osobaKlijentTableAdapter;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDServisiranjeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn iDKomponentaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn potrebnaKolicinaDataGridViewTextBoxColumn;
    }
}