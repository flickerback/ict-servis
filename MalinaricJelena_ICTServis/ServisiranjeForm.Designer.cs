﻿namespace MalinaricJelena_ICTServis
{
    partial class ServisiranjeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServisiranjeForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsb_new = new System.Windows.Forms.ToolStripButton();
            this.tsb_uredi = new System.Windows.Forms.ToolStripButton();
            this.dgv_servisiranje = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviserIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.osobaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ictServisDataSet1 = new MalinaricJelena_ICTServis.ICTServisDataSet();
            this.klijentIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.uredajIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.uredajBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.uslugaIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.uslugaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.datumPredajeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datumZavrsetkaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pokupljenoDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.servisiranjeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.servisiranjeTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.ServisiranjeTableAdapter();
            this.serviserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.zaposlenikTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.ZaposlenikTableAdapter();
            this.klijentBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.klijentTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.KlijentTableAdapter();
            this.uredajTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.UredajTableAdapter();
            this.uslugaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.UslugaTableAdapter();
            this.osobaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.OsobaTableAdapter();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_servisiranje)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osobaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uredajBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uslugaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servisiranjeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.klijentBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsb_new,
            this.tsb_uredi});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(860, 25);
            this.toolStrip1.TabIndex = 36;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsb_new
            // 
            this.tsb_new.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.tsb_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_new.Image = global::MalinaricJelena_ICTServis.Properties.Resources._new;
            this.tsb_new.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_new.Name = "tsb_new";
            this.tsb_new.Size = new System.Drawing.Size(54, 22);
            this.tsb_new.Text = "Novi";
            this.tsb_new.Click += new System.EventHandler(this.tsb_new_Click);
            // 
            // tsb_uredi
            // 
            this.tsb_uredi.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tsb_uredi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_uredi.Image = global::MalinaricJelena_ICTServis.Properties.Resources.save;
            this.tsb_uredi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_uredi.Name = "tsb_uredi";
            this.tsb_uredi.Size = new System.Drawing.Size(60, 22);
            this.tsb_uredi.Text = "Uredi";
            this.tsb_uredi.Click += new System.EventHandler(this.tsb_uredi_Click);
            // 
            // dgv_servisiranje
            // 
            this.dgv_servisiranje.AllowUserToAddRows = false;
            this.dgv_servisiranje.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_servisiranje.AutoGenerateColumns = false;
            this.dgv_servisiranje.BackgroundColor = System.Drawing.Color.MistyRose;
            this.dgv_servisiranje.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_servisiranje.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_servisiranje.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_servisiranje.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.serviserIDDataGridViewTextBoxColumn,
            this.klijentIDDataGridViewTextBoxColumn,
            this.uredajIDDataGridViewTextBoxColumn,
            this.uslugaIDDataGridViewTextBoxColumn,
            this.datumPredajeDataGridViewTextBoxColumn,
            this.datumZavrsetkaDataGridViewTextBoxColumn,
            this.pokupljenoDataGridViewCheckBoxColumn});
            this.dgv_servisiranje.DataSource = this.servisiranjeBindingSource;
            this.dgv_servisiranje.Location = new System.Drawing.Point(0, 28);
            this.dgv_servisiranje.Name = "dgv_servisiranje";
            this.dgv_servisiranje.ReadOnly = true;
            this.dgv_servisiranje.Size = new System.Drawing.Size(860, 510);
            this.dgv_servisiranje.TabIndex = 35;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.iDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // serviserIDDataGridViewTextBoxColumn
            // 
            this.serviserIDDataGridViewTextBoxColumn.DataPropertyName = "ServiserID";
            this.serviserIDDataGridViewTextBoxColumn.DataSource = this.osobaBindingSource;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.serviserIDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.serviserIDDataGridViewTextBoxColumn.DisplayMember = "PrezimeIme";
            this.serviserIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.serviserIDDataGridViewTextBoxColumn.HeaderText = "Serviser";
            this.serviserIDDataGridViewTextBoxColumn.Name = "serviserIDDataGridViewTextBoxColumn";
            this.serviserIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.serviserIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.serviserIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.serviserIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // osobaBindingSource
            // 
            this.osobaBindingSource.DataMember = "Osoba";
            this.osobaBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // ictServisDataSet1
            // 
            this.ictServisDataSet1.DataSetName = "ICTServisDataSet";
            this.ictServisDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // klijentIDDataGridViewTextBoxColumn
            // 
            this.klijentIDDataGridViewTextBoxColumn.DataPropertyName = "KlijentID";
            this.klijentIDDataGridViewTextBoxColumn.DataSource = this.osobaBindingSource;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.klijentIDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.klijentIDDataGridViewTextBoxColumn.DisplayMember = "PrezimeIme";
            this.klijentIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.klijentIDDataGridViewTextBoxColumn.HeaderText = "Klijent";
            this.klijentIDDataGridViewTextBoxColumn.Name = "klijentIDDataGridViewTextBoxColumn";
            this.klijentIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.klijentIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.klijentIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.klijentIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // uredajIDDataGridViewTextBoxColumn
            // 
            this.uredajIDDataGridViewTextBoxColumn.DataPropertyName = "UredajID";
            this.uredajIDDataGridViewTextBoxColumn.DataSource = this.uredajBindingSource;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.uredajIDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.uredajIDDataGridViewTextBoxColumn.DisplayMember = "Naziv";
            this.uredajIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.uredajIDDataGridViewTextBoxColumn.HeaderText = "Uređaj";
            this.uredajIDDataGridViewTextBoxColumn.Name = "uredajIDDataGridViewTextBoxColumn";
            this.uredajIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.uredajIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.uredajIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.uredajIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // uredajBindingSource
            // 
            this.uredajBindingSource.DataMember = "Uredaj";
            this.uredajBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // uslugaIDDataGridViewTextBoxColumn
            // 
            this.uslugaIDDataGridViewTextBoxColumn.DataPropertyName = "UslugaID";
            this.uslugaIDDataGridViewTextBoxColumn.DataSource = this.uslugaBindingSource;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.uslugaIDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.uslugaIDDataGridViewTextBoxColumn.DisplayMember = "Naziv";
            this.uslugaIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.uslugaIDDataGridViewTextBoxColumn.HeaderText = "Usluga";
            this.uslugaIDDataGridViewTextBoxColumn.Name = "uslugaIDDataGridViewTextBoxColumn";
            this.uslugaIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.uslugaIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.uslugaIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.uslugaIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // uslugaBindingSource
            // 
            this.uslugaBindingSource.DataMember = "Usluga";
            this.uslugaBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // datumPredajeDataGridViewTextBoxColumn
            // 
            this.datumPredajeDataGridViewTextBoxColumn.DataPropertyName = "DatumPredaje";
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.datumPredajeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.datumPredajeDataGridViewTextBoxColumn.HeaderText = "Datum predaje";
            this.datumPredajeDataGridViewTextBoxColumn.Name = "datumPredajeDataGridViewTextBoxColumn";
            this.datumPredajeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // datumZavrsetkaDataGridViewTextBoxColumn
            // 
            this.datumZavrsetkaDataGridViewTextBoxColumn.DataPropertyName = "DatumZavrsetka";
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.datumZavrsetkaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.datumZavrsetkaDataGridViewTextBoxColumn.HeaderText = "Datum zavrsetka";
            this.datumZavrsetkaDataGridViewTextBoxColumn.Name = "datumZavrsetkaDataGridViewTextBoxColumn";
            this.datumZavrsetkaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // pokupljenoDataGridViewCheckBoxColumn
            // 
            this.pokupljenoDataGridViewCheckBoxColumn.DataPropertyName = "Pokupljeno";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle9.NullValue = false;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pokupljenoDataGridViewCheckBoxColumn.DefaultCellStyle = dataGridViewCellStyle9;
            this.pokupljenoDataGridViewCheckBoxColumn.HeaderText = "Pokupljeno";
            this.pokupljenoDataGridViewCheckBoxColumn.Name = "pokupljenoDataGridViewCheckBoxColumn";
            this.pokupljenoDataGridViewCheckBoxColumn.ReadOnly = true;
            // 
            // servisiranjeBindingSource
            // 
            this.servisiranjeBindingSource.DataMember = "Servisiranje";
            this.servisiranjeBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // servisiranjeTableAdapter
            // 
            this.servisiranjeTableAdapter.ClearBeforeFill = true;
            // 
            // serviserBindingSource
            // 
            this.serviserBindingSource.DataMember = "Zaposlenik";
            this.serviserBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // zaposlenikTableAdapter
            // 
            this.zaposlenikTableAdapter.ClearBeforeFill = true;
            // 
            // klijentBindingSource
            // 
            this.klijentBindingSource.DataMember = "Klijent";
            this.klijentBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // klijentTableAdapter
            // 
            this.klijentTableAdapter.ClearBeforeFill = true;
            // 
            // uredajTableAdapter
            // 
            this.uredajTableAdapter.ClearBeforeFill = true;
            // 
            // uslugaTableAdapter
            // 
            this.uslugaTableAdapter.ClearBeforeFill = true;
            // 
            // osobaTableAdapter
            // 
            this.osobaTableAdapter.ClearBeforeFill = true;
            // 
            // ServisiranjeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MalinaricJelena_ICTServis.Properties.Resources.background_red;
            this.ClientSize = new System.Drawing.Size(860, 538);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dgv_servisiranje);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ServisiranjeForm";
            this.Text = "Servisiranje";
            this.Load += new System.EventHandler(this.ServisiranjeForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_servisiranje)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osobaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uredajBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uslugaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servisiranjeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.klijentBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsb_new;
        private System.Windows.Forms.ToolStripButton tsb_uredi;
        private System.Windows.Forms.DataGridView dgv_servisiranje;
        private ICTServisDataSet ictServisDataSet1;
        private System.Windows.Forms.BindingSource servisiranjeBindingSource;
        private ICTServisDataSetTableAdapters.ServisiranjeTableAdapter servisiranjeTableAdapter;
        private System.Windows.Forms.BindingSource serviserBindingSource;
        private ICTServisDataSetTableAdapters.ZaposlenikTableAdapter zaposlenikTableAdapter;
        private System.Windows.Forms.BindingSource klijentBindingSource;
        private ICTServisDataSetTableAdapters.KlijentTableAdapter klijentTableAdapter;
        private System.Windows.Forms.BindingSource uredajBindingSource;
        private ICTServisDataSetTableAdapters.UredajTableAdapter uredajTableAdapter;
        private System.Windows.Forms.BindingSource uslugaBindingSource;
        private ICTServisDataSetTableAdapters.UslugaTableAdapter uslugaTableAdapter;
        private System.Windows.Forms.BindingSource osobaBindingSource;
        private ICTServisDataSetTableAdapters.OsobaTableAdapter osobaTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn serviserIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn klijentIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn uredajIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn uslugaIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datumPredajeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datumZavrsetkaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn pokupljenoDataGridViewCheckBoxColumn;
    }
}