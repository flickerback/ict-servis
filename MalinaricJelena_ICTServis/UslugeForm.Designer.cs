﻿namespace MalinaricJelena_ICTServis
{
    partial class UslugeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UslugeForm));
            this.dgvUsluge = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nazivDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cijenaUslugeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.trosakUslugeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.trajanjeRadaDaniDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uslugaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ictServisDataSet1 = new MalinaricJelena_ICTServis.ICTServisDataSet();
            this.uslugaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.UslugaTableAdapter();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsb_spremi = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsluge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uslugaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvUsluge
            // 
            this.dgvUsluge.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvUsluge.AutoGenerateColumns = false;
            this.dgvUsluge.BackgroundColor = System.Drawing.Color.LightCyan;
            this.dgvUsluge.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUsluge.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvUsluge.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsluge.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.nazivDataGridViewTextBoxColumn,
            this.cijenaUslugeDataGridViewTextBoxColumn,
            this.trosakUslugeDataGridViewTextBoxColumn,
            this.trajanjeRadaDaniDataGridViewTextBoxColumn});
            this.dgvUsluge.DataSource = this.uslugaBindingSource;
            this.dgvUsluge.Location = new System.Drawing.Point(0, 28);
            this.dgvUsluge.Name = "dgvUsluge";
            this.dgvUsluge.Size = new System.Drawing.Size(469, 426);
            this.dgvUsluge.TabIndex = 79;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.iDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // nazivDataGridViewTextBoxColumn
            // 
            this.nazivDataGridViewTextBoxColumn.DataPropertyName = "Naziv";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.nazivDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.nazivDataGridViewTextBoxColumn.HeaderText = "Naziv usluge";
            this.nazivDataGridViewTextBoxColumn.Name = "nazivDataGridViewTextBoxColumn";
            // 
            // cijenaUslugeDataGridViewTextBoxColumn
            // 
            this.cijenaUslugeDataGridViewTextBoxColumn.DataPropertyName = "CijenaUsluge";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle4.Format = "C2";
            dataGridViewCellStyle4.NullValue = "-";
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cijenaUslugeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.cijenaUslugeDataGridViewTextBoxColumn.HeaderText = "Cijena usluge";
            this.cijenaUslugeDataGridViewTextBoxColumn.Name = "cijenaUslugeDataGridViewTextBoxColumn";
            // 
            // trosakUslugeDataGridViewTextBoxColumn
            // 
            this.trosakUslugeDataGridViewTextBoxColumn.DataPropertyName = "TrosakUsluge";
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle5.Format = "C2";
            dataGridViewCellStyle5.NullValue = "-";
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.trosakUslugeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.trosakUslugeDataGridViewTextBoxColumn.HeaderText = "Trosak usluge";
            this.trosakUslugeDataGridViewTextBoxColumn.Name = "trosakUslugeDataGridViewTextBoxColumn";
            // 
            // trajanjeRadaDaniDataGridViewTextBoxColumn
            // 
            this.trajanjeRadaDaniDataGridViewTextBoxColumn.DataPropertyName = "TrajanjeRadaDani";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle6.NullValue = null;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.trajanjeRadaDaniDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.trajanjeRadaDaniDataGridViewTextBoxColumn.HeaderText = "Dani rada";
            this.trajanjeRadaDaniDataGridViewTextBoxColumn.Name = "trajanjeRadaDaniDataGridViewTextBoxColumn";
            // 
            // uslugaBindingSource
            // 
            this.uslugaBindingSource.DataMember = "Usluga";
            this.uslugaBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // ictServisDataSet1
            // 
            this.ictServisDataSet1.DataSetName = "ICTServisDataSet";
            this.ictServisDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // uslugaTableAdapter
            // 
            this.uslugaTableAdapter.ClearBeforeFill = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsb_spremi});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(469, 25);
            this.toolStrip1.TabIndex = 82;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsb_spremi
            // 
            this.tsb_spremi.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tsb_spremi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_spremi.Image = global::MalinaricJelena_ICTServis.Properties.Resources.save;
            this.tsb_spremi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_spremi.Name = "tsb_spremi";
            this.tsb_spremi.Size = new System.Drawing.Size(70, 22);
            this.tsb_spremi.Text = "Spremi";
            this.tsb_spremi.Click += new System.EventHandler(this.tsb_spremi_Click);
            // 
            // UslugeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MalinaricJelena_ICTServis.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(469, 455);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dgvUsluge);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UslugeForm";
            this.Text = "Popis usluga";
            this.Load += new System.EventHandler(this.UslugeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsluge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uslugaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvUsluge;
        private ICTServisDataSet ictServisDataSet1;
        private System.Windows.Forms.BindingSource uslugaBindingSource;
        private ICTServisDataSetTableAdapters.UslugaTableAdapter uslugaTableAdapter;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsb_spremi;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nazivDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cijenaUslugeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn trosakUslugeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn trajanjeRadaDaniDataGridViewTextBoxColumn;
    }
}