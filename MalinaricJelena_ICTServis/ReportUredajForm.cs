﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class ReportUredajForm : Form
    {
        public ReportUredajForm()
        {
            InitializeComponent();
        }

        private void ReportUredajForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ICTServisDataSet.Uredaj' table. You can move, or remove it, as needed.
            this.UredajTableAdapter.Fill(this.ICTServisDataSet.Uredaj);

            this.reportViewer1.RefreshReport();
        }
    }
}
