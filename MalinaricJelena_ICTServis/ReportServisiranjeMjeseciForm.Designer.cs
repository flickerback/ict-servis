﻿namespace MalinaricJelena_ICTServis
{
    partial class ReportServisiranjeMjeseciForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportServisiranjeMjeseciForm));
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ICTServisDataSet = new MalinaricJelena_ICTServis.ICTServisDataSet();
            this.ServisiranjeMjeseciBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ServisiranjeMjeseciTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.ServisiranjeMjeseciTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.ICTServisDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServisiranjeMjeseciBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.ServisiranjeMjeseciBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "MalinaricJelena_ICTServis.Servisiranje.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(-2, -1);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(803, 447);
            this.reportViewer1.TabIndex = 0;
            // 
            // ICTServisDataSet
            // 
            this.ICTServisDataSet.DataSetName = "ICTServisDataSet";
            this.ICTServisDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ServisiranjeMjeseciBindingSource
            // 
            this.ServisiranjeMjeseciBindingSource.DataMember = "ServisiranjeMjeseci";
            this.ServisiranjeMjeseciBindingSource.DataSource = this.ICTServisDataSet;
            // 
            // ServisiranjeMjeseciTableAdapter
            // 
            this.ServisiranjeMjeseciTableAdapter.ClearBeforeFill = true;
            // 
            // ReportServisiranjeMjeseciForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.ClientSize = new System.Drawing.Size(799, 442);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReportServisiranjeMjeseciForm";
            this.Text = "Izvještaj - servisiranje po mjesecima";
            this.Load += new System.EventHandler(this.ReportServisiranjeMjeseciForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ICTServisDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ServisiranjeMjeseciBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource ServisiranjeMjeseciBindingSource;
        private ICTServisDataSet ICTServisDataSet;
        private ICTServisDataSetTableAdapters.ServisiranjeMjeseciTableAdapter ServisiranjeMjeseciTableAdapter;
    }
}