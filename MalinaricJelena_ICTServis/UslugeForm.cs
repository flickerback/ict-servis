﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class UslugeForm : Form
    {
        public UslugeForm()
        {
            InitializeComponent();
            uslugaTableAdapter.Adapter.RowUpdated += Adapter_RowUpdated;
        }
        void Adapter_RowUpdated(object sender, System.Data.OleDb.OleDbRowUpdatedEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {
                var cmd = new OleDbCommand("SELECT @@IDENTITY", e.Command.Connection);
                var id = (int)cmd.ExecuteScalar();
                e.Row["ID"] = id;
                e.Row.AcceptChanges();
            }
        }

        private void UslugeForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ictServisDataSet1.Usluga' table. You can move, or remove it, as needed.
            this.uslugaTableAdapter.Fill(this.ictServisDataSet1.Usluga);
        }

        private void tsb_spremi_Click(object sender, EventArgs e)
        {
            uslugaBindingSource.EndEdit();
            uslugaTableAdapter.Update(ictServisDataSet1.Usluga);
        }
    }
}
