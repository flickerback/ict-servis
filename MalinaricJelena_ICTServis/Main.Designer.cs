﻿namespace MalinaricJelena_ICTServis
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.tsbItem_PopisUsluga = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbItem_PopisKomponenti = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbItem_PopisUredaja = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbItem_PopisProizvodaca = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.popisSvihOsobaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.zaposleniciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.klijentiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.popisRadnihMjestaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lokacijeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripMenuItem_report_uredaji = new System.Windows.Forms.ToolStripMenuItem();
            this.brojServisiranjaPoMjesecimaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pb_background = new System.Windows.Forms.PictureBox();
            this.btn_uredaji = new System.Windows.Forms.Button();
            this.btn_komponente = new System.Windows.Forms.Button();
            this.btn_narudzbe = new System.Windows.Forms.Button();
            this.btn_servisiranje = new System.Windows.Forms.Button();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_background)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip2
            // 
            this.toolStrip2.BackColor = System.Drawing.Color.PaleTurquoise;
            this.toolStrip2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripDropDownButton2,
            this.toolStripDropDownButton3});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(941, 25);
            this.toolStrip2.TabIndex = 0;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbItem_PopisUsluga,
            this.toolStripSeparator1,
            this.tsbItem_PopisKomponenti,
            this.tsbItem_PopisUredaja,
            this.tsbItem_PopisProizvodaca});
            this.toolStripDropDownButton1.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.toolStripDropDownButton1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toolStripDropDownButton1.Image = global::MalinaricJelena_ICTServis.Properties.Resources.notebook;
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(90, 22);
            this.toolStripDropDownButton1.Text = "Šifrarnici";
            // 
            // tsbItem_PopisUsluga
            // 
            this.tsbItem_PopisUsluga.Name = "tsbItem_PopisUsluga";
            this.tsbItem_PopisUsluga.Size = new System.Drawing.Size(213, 22);
            this.tsbItem_PopisUsluga.Text = "Popis usluga";
            this.tsbItem_PopisUsluga.Click += new System.EventHandler(this.tsbItem_PopisUsluga_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(210, 6);
            // 
            // tsbItem_PopisKomponenti
            // 
            this.tsbItem_PopisKomponenti.Name = "tsbItem_PopisKomponenti";
            this.tsbItem_PopisKomponenti.Size = new System.Drawing.Size(213, 22);
            this.tsbItem_PopisKomponenti.Text = "Popis tipova komponenti";
            this.tsbItem_PopisKomponenti.Click += new System.EventHandler(this.tsbItem_PopisKomponenti_Click);
            // 
            // tsbItem_PopisUredaja
            // 
            this.tsbItem_PopisUredaja.Name = "tsbItem_PopisUredaja";
            this.tsbItem_PopisUredaja.Size = new System.Drawing.Size(213, 22);
            this.tsbItem_PopisUredaja.Text = "Popis tipova uređaja";
            this.tsbItem_PopisUredaja.Click += new System.EventHandler(this.tsbItem_PopisUredaja_Click);
            // 
            // tsbItem_PopisProizvodaca
            // 
            this.tsbItem_PopisProizvodaca.Name = "tsbItem_PopisProizvodaca";
            this.tsbItem_PopisProizvodaca.Size = new System.Drawing.Size(213, 22);
            this.tsbItem_PopisProizvodaca.Text = "Popis proizvođača";
            this.tsbItem_PopisProizvodaca.Click += new System.EventHandler(this.tsbItem_PopisProizvodaca_Click);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.popisSvihOsobaToolStripMenuItem,
            this.toolStripSeparator3,
            this.zaposleniciToolStripMenuItem,
            this.klijentiToolStripMenuItem,
            this.toolStripSeparator2,
            this.popisRadnihMjestaToolStripMenuItem,
            this.lokacijeToolStripMenuItem});
            this.toolStripDropDownButton2.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.toolStripDropDownButton2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toolStripDropDownButton2.Image = global::MalinaricJelena_ICTServis.Properties.Resources.user;
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(73, 22);
            this.toolStripDropDownButton2.Text = "Osobe";
            // 
            // popisSvihOsobaToolStripMenuItem
            // 
            this.popisSvihOsobaToolStripMenuItem.Name = "popisSvihOsobaToolStripMenuItem";
            this.popisSvihOsobaToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.popisSvihOsobaToolStripMenuItem.Text = "Popis svih osoba";
            this.popisSvihOsobaToolStripMenuItem.Click += new System.EventHandler(this.popisSvihOsobaToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(184, 6);
            // 
            // zaposleniciToolStripMenuItem
            // 
            this.zaposleniciToolStripMenuItem.Name = "zaposleniciToolStripMenuItem";
            this.zaposleniciToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.zaposleniciToolStripMenuItem.Text = "Zaposlenici";
            this.zaposleniciToolStripMenuItem.Click += new System.EventHandler(this.zaposleniciToolStripMenuItem_Click);
            // 
            // klijentiToolStripMenuItem
            // 
            this.klijentiToolStripMenuItem.Name = "klijentiToolStripMenuItem";
            this.klijentiToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.klijentiToolStripMenuItem.Text = "Klijenti";
            this.klijentiToolStripMenuItem.Click += new System.EventHandler(this.klijentiToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(184, 6);
            // 
            // popisRadnihMjestaToolStripMenuItem
            // 
            this.popisRadnihMjestaToolStripMenuItem.Name = "popisRadnihMjestaToolStripMenuItem";
            this.popisRadnihMjestaToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.popisRadnihMjestaToolStripMenuItem.Text = "Popis radnih mjesta";
            this.popisRadnihMjestaToolStripMenuItem.Click += new System.EventHandler(this.popisRadnihMjestaToolStripMenuItem_Click);
            // 
            // lokacijeToolStripMenuItem
            // 
            this.lokacijeToolStripMenuItem.Name = "lokacijeToolStripMenuItem";
            this.lokacijeToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.lokacijeToolStripMenuItem.Text = "Lokacije";
            this.lokacijeToolStripMenuItem.Click += new System.EventHandler(this.lokacijeToolStripMenuItem_Click);
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_report_uredaji,
            this.brojServisiranjaPoMjesecimaToolStripMenuItem});
            this.toolStripDropDownButton3.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripDropDownButton3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.toolStripDropDownButton3.Image = global::MalinaricJelena_ICTServis.Properties.Resources.checkmark;
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(88, 22);
            this.toolStripDropDownButton3.Text = "Izvještaji";
            // 
            // toolStripMenuItem_report_uredaji
            // 
            this.toolStripMenuItem_report_uredaji.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.toolStripMenuItem_report_uredaji.Name = "toolStripMenuItem_report_uredaji";
            this.toolStripMenuItem_report_uredaji.Size = new System.Drawing.Size(252, 22);
            this.toolStripMenuItem_report_uredaji.Text = "Popis uređaja";
            this.toolStripMenuItem_report_uredaji.Click += new System.EventHandler(this.toolStripMenuItem_report_uredaji_Click);
            // 
            // brojServisiranjaPoMjesecimaToolStripMenuItem
            // 
            this.brojServisiranjaPoMjesecimaToolStripMenuItem.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.brojServisiranjaPoMjesecimaToolStripMenuItem.Name = "brojServisiranjaPoMjesecimaToolStripMenuItem";
            this.brojServisiranjaPoMjesecimaToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.brojServisiranjaPoMjesecimaToolStripMenuItem.Text = "Broj servisiranja po mjesecima";
            this.brojServisiranjaPoMjesecimaToolStripMenuItem.Click += new System.EventHandler(this.brojServisiranjaPoMjesecimaToolStripMenuItem_Click);
            // 
            // pb_background
            // 
            this.pb_background.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pb_background.Image = global::MalinaricJelena_ICTServis.Properties.Resources.background_new;
            this.pb_background.Location = new System.Drawing.Point(0, 28);
            this.pb_background.Name = "pb_background";
            this.pb_background.Size = new System.Drawing.Size(948, 601);
            this.pb_background.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_background.TabIndex = 1;
            this.pb_background.TabStop = false;
            // 
            // btn_uredaji
            // 
            this.btn_uredaji.Font = new System.Drawing.Font("Bahnschrift SemiLight", 20.25F);
            this.btn_uredaji.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_uredaji.Image = global::MalinaricJelena_ICTServis.Properties.Resources.background_purple;
            this.btn_uredaji.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btn_uredaji.Location = new System.Drawing.Point(106, 160);
            this.btn_uredaji.Name = "btn_uredaji";
            this.btn_uredaji.Size = new System.Drawing.Size(190, 103);
            this.btn_uredaji.TabIndex = 2;
            this.btn_uredaji.Text = "Uređaji";
            this.btn_uredaji.UseVisualStyleBackColor = true;
            this.btn_uredaji.Click += new System.EventHandler(this.btn_uredaji_Click);
            // 
            // btn_komponente
            // 
            this.btn_komponente.Font = new System.Drawing.Font("Bahnschrift SemiLight", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_komponente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_komponente.Image = global::MalinaricJelena_ICTServis.Properties.Resources.background_purple;
            this.btn_komponente.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btn_komponente.Location = new System.Drawing.Point(106, 384);
            this.btn_komponente.Name = "btn_komponente";
            this.btn_komponente.Size = new System.Drawing.Size(190, 103);
            this.btn_komponente.TabIndex = 3;
            this.btn_komponente.Text = "Komponente";
            this.btn_komponente.UseVisualStyleBackColor = true;
            this.btn_komponente.Click += new System.EventHandler(this.btn_komponente_Click);
            // 
            // btn_narudzbe
            // 
            this.btn_narudzbe.Font = new System.Drawing.Font("Bahnschrift SemiLight", 20.25F);
            this.btn_narudzbe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_narudzbe.Image = global::MalinaricJelena_ICTServis.Properties.Resources.background_red;
            this.btn_narudzbe.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btn_narudzbe.Location = new System.Drawing.Point(612, 160);
            this.btn_narudzbe.Name = "btn_narudzbe";
            this.btn_narudzbe.Size = new System.Drawing.Size(190, 103);
            this.btn_narudzbe.TabIndex = 4;
            this.btn_narudzbe.Text = "Naručivanje";
            this.btn_narudzbe.UseVisualStyleBackColor = true;
            this.btn_narudzbe.Click += new System.EventHandler(this.btn_narudzbe_Click);
            // 
            // btn_servisiranje
            // 
            this.btn_servisiranje.Font = new System.Drawing.Font("Bahnschrift SemiLight", 20.25F);
            this.btn_servisiranje.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btn_servisiranje.Image = global::MalinaricJelena_ICTServis.Properties.Resources.background_red;
            this.btn_servisiranje.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btn_servisiranje.Location = new System.Drawing.Point(612, 384);
            this.btn_servisiranje.Name = "btn_servisiranje";
            this.btn_servisiranje.Size = new System.Drawing.Size(190, 103);
            this.btn_servisiranje.TabIndex = 5;
            this.btn_servisiranje.Text = "Servisiranje";
            this.btn_servisiranje.UseVisualStyleBackColor = true;
            this.btn_servisiranje.Click += new System.EventHandler(this.btn_servisiranje_Click);
            // 
            // Main
            // 
            this.ClientSize = new System.Drawing.Size(941, 630);
            this.Controls.Add(this.btn_servisiranje);
            this.Controls.Add(this.btn_narudzbe);
            this.Controls.Add(this.btn_komponente);
            this.Controls.Add(this.btn_uredaji);
            this.Controls.Add(this.pb_background);
            this.Controls.Add(this.toolStrip2);
            this.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "ICT Servis Management Studio";
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_background)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton tsb_sifrarnici;
        private System.Windows.Forms.ToolStripMenuItem popisUslugaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem popisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem popisUređajaToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem tsbItem_PopisUsluga;
        private System.Windows.Forms.ToolStripMenuItem tsbItem_PopisKomponenti;
        private System.Windows.Forms.ToolStripMenuItem tsbItem_PopisUredaja;
        private System.Windows.Forms.ToolStripMenuItem tsbItem_PopisProizvodaca;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem popisRadnihMjestaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lokacijeToolStripMenuItem;
        private System.Windows.Forms.PictureBox pb_background;
        private System.Windows.Forms.Button btn_uredaji;
        private System.Windows.Forms.Button btn_komponente;
        private System.Windows.Forms.ToolStripMenuItem zaposleniciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem popisSvihOsobaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem klijentiToolStripMenuItem;
        private System.Windows.Forms.Button btn_narudzbe;
        private System.Windows.Forms.Button btn_servisiranje;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_report_uredaji;
        private System.Windows.Forms.ToolStripMenuItem brojServisiranjaPoMjesecimaToolStripMenuItem;
    }
}

