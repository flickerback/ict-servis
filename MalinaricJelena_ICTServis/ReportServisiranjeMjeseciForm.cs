﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class ReportServisiranjeMjeseciForm : Form
    {
        public ReportServisiranjeMjeseciForm()
        {
            InitializeComponent();
        }

        private void ReportServisiranjeMjeseciForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ICTServisDataSet.ServisiranjeMjeseci' table. You can move, or remove it, as needed.
            this.ServisiranjeMjeseciTableAdapter.Fill(this.ICTServisDataSet.ServisiranjeMjeseci);

            this.reportViewer1.RefreshReport();
        }
    }
}
