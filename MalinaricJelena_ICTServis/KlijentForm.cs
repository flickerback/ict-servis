﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class KlijentForm : Form
    {
        public KlijentForm()
        {
            InitializeComponent();
            klijentTableAdapter.Adapter.RowUpdated += Adapter_RowUpdated;
        }
        void Adapter_RowUpdated(object sender, System.Data.OleDb.OleDbRowUpdatedEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {

                var cmd = new OleDbCommand("SELECT @@IDENTITY", e.Command.Connection);
                var id = (int)cmd.ExecuteScalar();
                e.Row["OsobaID"] = id;
                e.Row.AcceptChanges();

            }
        }

        private void KlijentForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ictServisDataSet1.Osoba' table. You can move, or remove it, as needed.
            this.osobaTableAdapter.Fill(this.ictServisDataSet1.Osoba);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Klijent' table. You can move, or remove it, as needed.
            this.klijentTableAdapter.Fill(this.ictServisDataSet1.Klijent);

        }

        private void tsb_new_Click(object sender, EventArgs e)
        {
            try
            {
                var row = ictServisDataSet1.Klijent.NewKlijentRow();
                var randomID = (int)osobaTableAdapter.RandomID();
                row.OsobaID = randomID;
                ictServisDataSet1.Klijent.AddKlijentRow(row);
            }
            catch (Exception ex)
            {
                const string message = "Registrirajte novu osobu!";
                const string caption = "Greška";
                var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    var osobaForm = new OsobaForm();
                    osobaForm.FormClosed += refreshDataGridView;
                    osobaForm.ShowDialog();
                }

            }
        }

        private void tsb_novaOsoba_Click(object sender, EventArgs e)
        {
            var osobaForm = new OsobaForm();
            osobaForm.ShowDialog();
        }

        private void tsb_spremi_Click(object sender, EventArgs e)
        {
            try
            {
                klijentBindingSource.EndEdit();
                klijentTableAdapter.Update(ictServisDataSet1.Klijent);
                klijentTableAdapter.Fill(ictServisDataSet1.Klijent);
            }
            catch (Exception ex)
            {


            }
        }

        private void tsbObrisi_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Jeste li sigurni da želite izbrisati klijenta?", "Upit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
DialogResult.Yes)
            {
                ictServisDataSet1.Klijent[dataGridView1.CurrentRow.Index].Delete();
                try
                {
                    klijentTableAdapter.Update(ictServisDataSet1.Klijent);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Pozor!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
        private void refreshDataGridView(object sender, FormClosedEventArgs e)
        {
            osobaTableAdapter.Update(ictServisDataSet1.Osoba);
            osobaTableAdapter.Fill(ictServisDataSet1.Osoba);
            klijentTableAdapter.Update(ictServisDataSet1.Klijent);
            klijentTableAdapter.Fill(ictServisDataSet1.Klijent);
            dataGridView1.Refresh();
        }
    }
}
