﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class ServisiranjeForm : Form
    {
        public ServisiranjeForm()
        {
            InitializeComponent();
        }

        private void ServisiranjeForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ictServisDataSet1.Osoba' table. You can move, or remove it, as needed.
            this.osobaTableAdapter.Fill(this.ictServisDataSet1.Osoba);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Usluga' table. You can move, or remove it, as needed.
            this.uslugaTableAdapter.Fill(this.ictServisDataSet1.Usluga);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Uredaj' table. You can move, or remove it, as needed.
            this.uredajTableAdapter.Fill(this.ictServisDataSet1.Uredaj);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Klijent' table. You can move, or remove it, as needed.
            this.klijentTableAdapter.Fill(this.ictServisDataSet1.Klijent);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Zaposlenik' table. You can move, or remove it, as needed.
            this.zaposlenikTableAdapter.Fill(this.ictServisDataSet1.Zaposlenik);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Servisiranje' table. You can move, or remove it, as needed.
            this.servisiranjeTableAdapter.Fill(this.ictServisDataSet1.Servisiranje);

        }

        private void tsb_uredi_Click(object sender, EventArgs e)
        {
            var f = new ServisiranjePojedinacnoForm();
            f.Id = (int)dgv_servisiranje.CurrentRow.Cells[0].Value;
            if (f.ShowDialog() == DialogResult.OK)
                servisiranjeTableAdapter.Fill(ictServisDataSet1.Servisiranje);

        }

        private void tsb_new_Click(object sender, EventArgs e)
        {
            var unosForma = new ServisiranjePojedinacnoForm();
            unosForma.Id = 0;
            var rezultat = unosForma.ShowDialog();
            if (rezultat == DialogResult.OK)
                this.servisiranjeTableAdapter.Fill(this.ictServisDataSet1.Servisiranje);
        }
    }
}
