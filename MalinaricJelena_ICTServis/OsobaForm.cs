﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class OsobaForm : Form
    {
        public OsobaForm()
        {
            InitializeComponent();
            osobaTableAdapter.Adapter.RowUpdated += Adapter_RowUpdated;
        }
        void Adapter_RowUpdated(object sender, System.Data.OleDb.OleDbRowUpdatedEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {

                var cmd = new OleDbCommand("SELECT @@IDENTITY", e.Command.Connection);
                var id = (int)cmd.ExecuteScalar();
                e.Row["ID"] = id;
                e.Row.AcceptChanges();

            }
        }

        private void OsobaForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ictServisDataSet1.Mjesto' table. You can move, or remove it, as needed.
            this.mjestoTableAdapter.Fill(this.ictServisDataSet1.Mjesto);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Osoba' table. You can move, or remove it, as needed.
            this.osobaTableAdapter.Fill(this.ictServisDataSet1.Osoba);

        }

        private void tsb_novi_Click(object sender, EventArgs e)
        {
            var row = ictServisDataSet1.Osoba.NewOsobaRow();
            ictServisDataSet1.Osoba.AddOsobaRow(row);
        }

        private void tsb_spremi_Click(object sender, EventArgs e)
        {
            osobaBindingSource.EndEdit();
            osobaTableAdapter.Update(ictServisDataSet1.Osoba);
        }

        private void tsbObrisi_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Jeste li sigurni da želite izbrisati osobu?", "Upit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                 DialogResult.Yes)
            {
                ictServisDataSet1.Osoba[dataGridView1.CurrentRow.Index].Delete();
                try
                {
                    osobaTableAdapter.Update(ictServisDataSet1.Osoba);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Pozor!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }

        }
    }
}
