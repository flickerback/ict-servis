﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class KomponenteForm : Form
    {
        public KomponenteForm()
        {
            InitializeComponent();
            komponentaTableAdapter.Adapter.RowUpdated += Adapter_RowUpdated;
        }
        void Adapter_RowUpdated(object sender, System.Data.OleDb.OleDbRowUpdatedEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {
                var cmd = new OleDbCommand("SELECT @@IDENTITY", e.Command.Connection);
                var id = (int)cmd.ExecuteScalar();
                e.Row["ID"] = id;
                e.Row.AcceptChanges();
            }
        }

        private void KomponenteForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ictServisDataSet1.Komponenta' table. You can move, or remove it, as needed.
            this.komponentaTableAdapter.Fill(this.ictServisDataSet1.Komponenta);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Proizvodac' table. You can move, or remove it, as needed.
            this.proizvodacTableAdapter.Fill(this.ictServisDataSet1.Proizvodac);
            // TODO: This line of code loads data into the 'ictServisDataSet1.VrstaKomponente' table. You can move, or remove it, as needed.
            this.vrstaKomponenteTableAdapter.Fill(this.ictServisDataSet1.VrstaKomponente);

        }

        private void tsb_new_Click(object sender, EventArgs e)
        {
            var row = ictServisDataSet1.Komponenta.NewKomponentaRow();
            ictServisDataSet1.Komponenta.AddKomponentaRow(row);
        }

        private void tsb_spremi_Click(object sender, EventArgs e)
        {
            komponentaBindingSource.EndEdit();
            komponentaTableAdapter.Update(ictServisDataSet1.Komponenta);
        }

    }
}
