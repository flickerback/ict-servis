﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void tsbItem_PopisUsluga_Click(object sender, EventArgs e)
        {
            var uslugeForma = new UslugeForm();
            uslugeForma.ShowDialog();
        }

        private void tsbItem_PopisKomponenti_Click(object sender, EventArgs e)
        {
            var vrsteKomponentiForma = new VrsteKomponentiForm();
            vrsteKomponentiForma.ShowDialog();
        }

        private void tsbItem_PopisUredaja_Click(object sender, EventArgs e)
        {
            var vrstaUredajaForma = new VrsteUredajaForm();
            vrstaUredajaForma.ShowDialog();
        }

        private void tsbItem_PopisProizvodaca_Click(object sender, EventArgs e)
        {
            var proizvodacForma = new ProizvodacForm();
            proizvodacForma.ShowDialog();
        }

        private void popisRadnihMjestaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var radnaMjestaForma = new RadnaMjestaForm();
            radnaMjestaForma.ShowDialog();
        }

        private void lokacijeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var lokacijeForma = new LokacijeForm();
            lokacijeForma.ShowDialog();
        }

        private void btn_uredaji_Click(object sender, EventArgs e)
        {
            var uredajiForma = new UredajForm();
            uredajiForma.ShowDialog();
        }

        private void btn_komponente_Click(object sender, EventArgs e)
        {
            var komponenteForma = new KomponenteForm();
            komponenteForma.ShowDialog();
        }

        private void zaposleniciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var zaposleniciForma = new ZaposleniciForm();
            zaposleniciForma.ShowDialog();
        }

        private void popisSvihOsobaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var osobeForma = new OsobaForm();
            osobeForma.ShowDialog();
        }

        private void klijentiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var klijentiForma = new KlijentForm();
            klijentiForma.ShowDialog();
        }

        private void btn_narudzbe_Click(object sender, EventArgs e)
        {
            var narudzbeForma = new NarudzbeForm();
            narudzbeForma.ShowDialog();
        }

        private void btn_servisiranje_Click(object sender, EventArgs e)
        {
            var servisiranjeForma = new ServisiranjeForm();
            servisiranjeForma.ShowDialog();
        }

        private void toolStripMenuItem_report_uredaji_Click(object sender, EventArgs e)
        {
            var izvjestajUredajiForma = new ReportUredajForm();
            izvjestajUredajiForma.ShowDialog();
        }

        private void brojServisiranjaPoMjesecimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var izvjestajServisiranjeMjeseciForma = new ReportServisiranjeMjeseciForm();
            izvjestajServisiranjeMjeseciForma.ShowDialog();
        }
    }
}
