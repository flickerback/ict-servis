﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class ZaposleniciForm : Form
    {
        public ZaposleniciForm()
        {
            InitializeComponent();
            zaposlenikTableAdapter.Adapter.RowUpdated += Adapter_RowUpdated;
        }
        void Adapter_RowUpdated(object sender, System.Data.OleDb.OleDbRowUpdatedEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {

                var cmd = new OleDbCommand("SELECT @@IDENTITY", e.Command.Connection);
                var id = (int)cmd.ExecuteScalar();
                e.Row["OsobaID"] = id;
                e.Row.AcceptChanges();

            }
        }

        private void ZaposleniciForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ictServisDataSet1.Mjesto' table. You can move, or remove it, as needed.
            this.mjestoTableAdapter.Fill(this.ictServisDataSet1.Mjesto);
            // TODO: This line of code loads data into the 'ictServisDataSet1.RadnaMjesta' table. You can move, or remove it, as needed.
            this.radnaMjestaTableAdapter.Fill(this.ictServisDataSet1.RadnaMjesta);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Osoba' table. You can move, or remove it, as needed.
            this.osobaTableAdapter.Fill(this.ictServisDataSet1.Osoba);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Zaposlenik' table. You can move, or remove it, as needed.
            this.zaposlenikTableAdapter.Fill(this.ictServisDataSet1.Zaposlenik);
            // TODO: This line of code loads data into the 'iCTServisDataSet.RadnaMjesta' table. You can move, or remove it, as needed.

        }

        private void tsb_spremi_Click(object sender, EventArgs e)
        {
            try
            {
                zaposlenikBindingSource.EndEdit();
                zaposlenikTableAdapter.Update(ictServisDataSet1.Zaposlenik);
                zaposlenikTableAdapter.Fill(ictServisDataSet1.Zaposlenik);
            }
            catch (Exception ex)
            {

                
            }
        }

        private void tsb_new_Click(object sender, EventArgs e)
        {
            try
            {
                var row = ictServisDataSet1.Zaposlenik.NewZaposlenikRow();
                var randomID = (int)osobaTableAdapter.RandomID();
                row.OsobaID = randomID;
                row.RadnoMjestoID = 1;
                ictServisDataSet1.Zaposlenik.AddZaposlenikRow(row);
            }
            catch (Exception ex)
            {
                const string message = "Registrirajte novu osobu!";
                const string caption = "Greška";
                var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    var osobaForm = new OsobaForm();
                    osobaForm.FormClosed += refreshDataGridView;
                    osobaForm.ShowDialog();
                }

            }
        }

        private void tsb_novaOsoba_Click(object sender, EventArgs e)
        {
            var osobaForm = new OsobaForm();
            osobaForm.ShowDialog();
        }
        private void refreshDataGridView(object sender, FormClosedEventArgs e)
        {
            osobaTableAdapter.Update(ictServisDataSet1.Osoba);
            osobaTableAdapter.Fill(ictServisDataSet1.Osoba);
            zaposlenikTableAdapter.Update(ictServisDataSet1.Zaposlenik);
            zaposlenikTableAdapter.Fill(ictServisDataSet1.Zaposlenik);
            dataGridView1.Refresh();
        }

        private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }

        private void tsbObrisi_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Jeste li sigurni da želite izbrisati zaposlenika?", "Upit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
     DialogResult.Yes)
            {
                ictServisDataSet1.Zaposlenik[dataGridView1.CurrentRow.Index].Delete();
                try
                {
                    zaposlenikTableAdapter.Update(ictServisDataSet1.Zaposlenik);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Pozor!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
        }
    }
}
