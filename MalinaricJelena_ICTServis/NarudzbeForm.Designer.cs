﻿namespace MalinaricJelena_ICTServis
{
    partial class NarudzbeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NarudzbeForm));
            this.label5 = new System.Windows.Forms.Label();
            this.txtCijena = new System.Windows.Forms.TextBox();
            this.narucivanjeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ictServisDataSet1 = new MalinaricJelena_ICTServis.ICTServisDataSet();
            this.cb_komponenta = new System.Windows.Forms.ComboBox();
            this.komponentaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cb_narucitelj = new System.Windows.Forms.ComboBox();
            this.osobaZaposlenikBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.osobaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.zaposlenikBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsb_new = new System.Windows.Forms.ToolStripButton();
            this.tsb_spremi = new System.Windows.Forms.ToolStripButton();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.naruciteljIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.komponentaIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.kolicinaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datumNarudzbeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtp_datumNarudzbe = new System.Windows.Forms.DateTimePicker();
            this.narucivanjeTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.NarucivanjeTableAdapter();
            this.zaposlenikTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.ZaposlenikTableAdapter();
            this.komponentaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.KomponentaTableAdapter();
            this.osobaTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.OsobaTableAdapter();
            this.osobaZaposlenikTableAdapter = new MalinaricJelena_ICTServis.ICTServisDataSetTableAdapters.OsobaZaposlenikTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.narucivanjeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.komponentaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osobaZaposlenikBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osobaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zaposlenikBindingSource)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label5.Location = new System.Drawing.Point(262, 352);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 16);
            this.label5.TabIndex = 33;
            this.label5.Text = "Količina:";
            // 
            // txtCijena
            // 
            this.txtCijena.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.narucivanjeBindingSource, "Kolicina", true));
            this.txtCijena.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.txtCijena.Location = new System.Drawing.Point(261, 372);
            this.txtCijena.Name = "txtCijena";
            this.txtCijena.Size = new System.Drawing.Size(175, 23);
            this.txtCijena.TabIndex = 32;
            // 
            // narucivanjeBindingSource
            // 
            this.narucivanjeBindingSource.DataMember = "Narucivanje";
            this.narucivanjeBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // ictServisDataSet1
            // 
            this.ictServisDataSet1.DataSetName = "ICTServisDataSet";
            this.ictServisDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cb_komponenta
            // 
            this.cb_komponenta.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.narucivanjeBindingSource, "KomponentaID", true));
            this.cb_komponenta.DataSource = this.komponentaBindingSource;
            this.cb_komponenta.DisplayMember = "Naziv";
            this.cb_komponenta.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            this.cb_komponenta.FormattingEnabled = true;
            this.cb_komponenta.Location = new System.Drawing.Point(27, 445);
            this.cb_komponenta.Name = "cb_komponenta";
            this.cb_komponenta.Size = new System.Drawing.Size(175, 24);
            this.cb_komponenta.TabIndex = 31;
            this.cb_komponenta.ValueMember = "ID";
            // 
            // komponentaBindingSource
            // 
            this.komponentaBindingSource.DataMember = "Komponenta";
            this.komponentaBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // cb_narucitelj
            // 
            this.cb_narucitelj.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.narucivanjeBindingSource, "NaruciteljID", true));
            this.cb_narucitelj.DataSource = this.osobaZaposlenikBindingSource;
            this.cb_narucitelj.DisplayMember = "PrezimeIme";
            this.cb_narucitelj.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_narucitelj.FormattingEnabled = true;
            this.cb_narucitelj.Location = new System.Drawing.Point(26, 371);
            this.cb_narucitelj.Name = "cb_narucitelj";
            this.cb_narucitelj.Size = new System.Drawing.Size(175, 24);
            this.cb_narucitelj.TabIndex = 30;
            this.cb_narucitelj.ValueMember = "ID";
            // 
            // osobaZaposlenikBindingSource
            // 
            this.osobaZaposlenikBindingSource.DataMember = "OsobaZaposlenik";
            this.osobaZaposlenikBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // osobaBindingSource
            // 
            this.osobaBindingSource.DataMember = "Osoba";
            this.osobaBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // zaposlenikBindingSource
            // 
            this.zaposlenikBindingSource.DataMember = "Zaposlenik";
            this.zaposlenikBindingSource.DataSource = this.ictServisDataSet1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label3.Location = new System.Drawing.Point(262, 426);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 16);
            this.label3.TabIndex = 29;
            this.label3.Text = "Naziv:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.label4.Location = new System.Drawing.Point(24, 426);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 16);
            this.label4.TabIndex = 27;
            this.label4.Text = "Komponenta:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(24, 352);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 24;
            this.label1.Text = "Naručitelj";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsb_new,
            this.tsb_spremi});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(540, 25);
            this.toolStrip1.TabIndex = 23;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsb_new
            // 
            this.tsb_new.Font = new System.Drawing.Font("Bahnschrift", 9.75F);
            this.tsb_new.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_new.Image = global::MalinaricJelena_ICTServis.Properties.Resources._new;
            this.tsb_new.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_new.Name = "tsb_new";
            this.tsb_new.Size = new System.Drawing.Size(54, 22);
            this.tsb_new.Text = "Novi";
            this.tsb_new.Click += new System.EventHandler(this.tsb_new_Click);
            // 
            // tsb_spremi
            // 
            this.tsb_spremi.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tsb_spremi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tsb_spremi.Image = global::MalinaricJelena_ICTServis.Properties.Resources.save;
            this.tsb_spremi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsb_spremi.Name = "tsb_spremi";
            this.tsb_spremi.Size = new System.Drawing.Size(70, 22);
            this.tsb_spremi.Text = "Spremi";
            this.tsb_spremi.Click += new System.EventHandler(this.tsb_spremi_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.MistyRose;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.naruciteljIDDataGridViewTextBoxColumn,
            this.komponentaIDDataGridViewTextBoxColumn,
            this.kolicinaDataGridViewTextBoxColumn,
            this.datumNarudzbeDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.narucivanjeBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(540, 290);
            this.dataGridView1.TabIndex = 22;
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.iDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDDataGridViewTextBoxColumn.Visible = false;
            // 
            // naruciteljIDDataGridViewTextBoxColumn
            // 
            this.naruciteljIDDataGridViewTextBoxColumn.DataPropertyName = "NaruciteljID";
            this.naruciteljIDDataGridViewTextBoxColumn.DataSource = this.osobaBindingSource;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.naruciteljIDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.naruciteljIDDataGridViewTextBoxColumn.DisplayMember = "PrezimeIme";
            this.naruciteljIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.naruciteljIDDataGridViewTextBoxColumn.HeaderText = "Naručitelj";
            this.naruciteljIDDataGridViewTextBoxColumn.Name = "naruciteljIDDataGridViewTextBoxColumn";
            this.naruciteljIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.naruciteljIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.naruciteljIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.naruciteljIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // komponentaIDDataGridViewTextBoxColumn
            // 
            this.komponentaIDDataGridViewTextBoxColumn.DataPropertyName = "KomponentaID";
            this.komponentaIDDataGridViewTextBoxColumn.DataSource = this.komponentaBindingSource;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.komponentaIDDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.komponentaIDDataGridViewTextBoxColumn.DisplayMember = "Naziv";
            this.komponentaIDDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.komponentaIDDataGridViewTextBoxColumn.HeaderText = "Komponenta";
            this.komponentaIDDataGridViewTextBoxColumn.Name = "komponentaIDDataGridViewTextBoxColumn";
            this.komponentaIDDataGridViewTextBoxColumn.ReadOnly = true;
            this.komponentaIDDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.komponentaIDDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.komponentaIDDataGridViewTextBoxColumn.ValueMember = "ID";
            // 
            // kolicinaDataGridViewTextBoxColumn
            // 
            this.kolicinaDataGridViewTextBoxColumn.DataPropertyName = "Kolicina";
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F);
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.kolicinaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.kolicinaDataGridViewTextBoxColumn.HeaderText = "Količina";
            this.kolicinaDataGridViewTextBoxColumn.Name = "kolicinaDataGridViewTextBoxColumn";
            this.kolicinaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // datumNarudzbeDataGridViewTextBoxColumn
            // 
            this.datumNarudzbeDataGridViewTextBoxColumn.DataPropertyName = "DatumNarudzbe";
            this.datumNarudzbeDataGridViewTextBoxColumn.HeaderText = "Datum narudžbe";
            this.datumNarudzbeDataGridViewTextBoxColumn.Name = "datumNarudzbeDataGridViewTextBoxColumn";
            this.datumNarudzbeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtp_datumNarudzbe
            // 
            this.dtp_datumNarudzbe.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.narucivanjeBindingSource, "DatumNarudzbe", true));
            this.dtp_datumNarudzbe.Font = new System.Drawing.Font("Bahnschrift Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dtp_datumNarudzbe.Location = new System.Drawing.Point(261, 446);
            this.dtp_datumNarudzbe.Name = "dtp_datumNarudzbe";
            this.dtp_datumNarudzbe.Size = new System.Drawing.Size(175, 23);
            this.dtp_datumNarudzbe.TabIndex = 34;
            // 
            // narucivanjeTableAdapter
            // 
            this.narucivanjeTableAdapter.ClearBeforeFill = true;
            // 
            // zaposlenikTableAdapter
            // 
            this.zaposlenikTableAdapter.ClearBeforeFill = true;
            // 
            // komponentaTableAdapter
            // 
            this.komponentaTableAdapter.ClearBeforeFill = true;
            // 
            // osobaTableAdapter
            // 
            this.osobaTableAdapter.ClearBeforeFill = true;
            // 
            // osobaZaposlenikTableAdapter
            // 
            this.osobaZaposlenikTableAdapter.ClearBeforeFill = true;
            // 
            // NarudzbeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::MalinaricJelena_ICTServis.Properties.Resources.background_red;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(540, 511);
            this.Controls.Add(this.dtp_datumNarudzbe);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCijena);
            this.Controls.Add(this.cb_komponenta);
            this.Controls.Add(this.cb_narucitelj);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dataGridView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NarudzbeForm";
            this.Text = "Naručivanje";
            this.Load += new System.EventHandler(this.NarudzbeForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.narucivanjeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ictServisDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.komponentaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osobaZaposlenikBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osobaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zaposlenikBindingSource)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCijena;
        private System.Windows.Forms.ComboBox cb_komponenta;
        private System.Windows.Forms.ComboBox cb_narucitelj;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsb_new;
        private System.Windows.Forms.ToolStripButton tsb_spremi;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DateTimePicker dtp_datumNarudzbe;
        private ICTServisDataSet ictServisDataSet1;
        private System.Windows.Forms.BindingSource narucivanjeBindingSource;
        private System.Windows.Forms.BindingSource zaposlenikBindingSource;
        private System.Windows.Forms.BindingSource komponentaBindingSource;
        private ICTServisDataSetTableAdapters.NarucivanjeTableAdapter narucivanjeTableAdapter;
        private ICTServisDataSetTableAdapters.ZaposlenikTableAdapter zaposlenikTableAdapter;
        private ICTServisDataSetTableAdapters.KomponentaTableAdapter komponentaTableAdapter;
        private System.Windows.Forms.BindingSource osobaBindingSource;
        private ICTServisDataSetTableAdapters.OsobaTableAdapter osobaTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn naruciteljIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn komponentaIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn kolicinaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datumNarudzbeDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource osobaZaposlenikBindingSource;
        private ICTServisDataSetTableAdapters.OsobaZaposlenikTableAdapter osobaZaposlenikTableAdapter;
    }
}