﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MalinaricJelena_ICTServis
{
    public partial class NarudzbeForm : Form
    {
        public NarudzbeForm()
        {
            InitializeComponent();
            narucivanjeTableAdapter.Adapter.RowUpdated += Adapter_RowUpdated;
        }
        void Adapter_RowUpdated(object sender, System.Data.OleDb.OleDbRowUpdatedEventArgs e)
        {
            if (e.StatementType == StatementType.Insert)
            {
                var cmd = new OleDbCommand("SELECT @@IDENTITY", e.Command.Connection);
                var id = (int)cmd.ExecuteScalar();
                e.Row["ID"] = id;
                e.Row.AcceptChanges();
            }
        }

        private void NarudzbeForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'iCTServisDataSet.OsobaZaposlenik' table. You can move, or remove it, as needed.
            this.osobaZaposlenikTableAdapter.Fill(this.ictServisDataSet1.OsobaZaposlenik);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Zaposlenik' table. You can move, or remove it, as needed.
            this.zaposlenikTableAdapter.Fill(this.ictServisDataSet1.Zaposlenik);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Osoba' table. You can move, or remove it, as needed.
            this.osobaTableAdapter.Fill(this.ictServisDataSet1.Osoba);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Komponenta' table. You can move, or remove it, as needed.
            this.komponentaTableAdapter.Fill(this.ictServisDataSet1.Komponenta);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Zaposlenik' table. You can move, or remove it, as needed.
            this.zaposlenikTableAdapter.Fill(this.ictServisDataSet1.Zaposlenik);
            // TODO: This line of code loads data into the 'ictServisDataSet1.Narucivanje' table. You can move, or remove it, as needed.
            this.narucivanjeTableAdapter.Fill(this.ictServisDataSet1.Narucivanje);
            // TODO: This line of code loads data into the 'iCTServisDataSet.Zaposlenik' table. You can move, or remove it, as needed..
            this.narucivanjeTableAdapter.Fill(this.ictServisDataSet1.Narucivanje);

        }

        private void tsb_new_Click(object sender, EventArgs e)
        {
            var row = ictServisDataSet1.Narucivanje.NewNarucivanjeRow();
            ictServisDataSet1.Narucivanje.AddNarucivanjeRow(row);
        }

        private void tsb_spremi_Click(object sender, EventArgs e)
        {
            narucivanjeBindingSource.EndEdit();
            narucivanjeTableAdapter.Update(ictServisDataSet1.Narucivanje);
        }
    }
}
